function [kappa,c,nexp,lx,u0,alpha_realstep,C1,ss_yield] = reset_params
%RESET Summary of this function goes here
%   Detailed explanation goes here
    ss_yield = 1;
    kappa = 1e-2;   % linear diffusion coefficient
    c     = 0.01;   % discharge transport coefficient
    nexp  = 1;      % discharge exponent
    lx = 2e3;       % system length metres
    u0 = 1e-3;      % uplift m/yr
    alpha_realstep = 1e3; % alpha timestep
    C1 = 0.7;
end

