clear;
load('./data/pico_glacial_interglacial.mat');

alpha = glacial_sim_rainfall.mean_rainfall_with_variance.t05;

[kappa,c,nexp,lx,u0,alpha_realstep,C1,ss_yield] = reset_params;
alpha_realstep = 100;
lx = 3000;
nexp = 1;
c = 0.01;
kappa = 0.005;
u0 = 0.002;
C1 = 0.7;
tmax = length(alpha);
ss_alpha_condition = alpha(1);
run_name = 'with_variance';
fan_evo;
save('./data/with_variance.mat', '-v7.3');