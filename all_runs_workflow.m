
addpath('./processing');
addpath('./sensitivity_analyses');
addpath('./weather_station');

load('./weather_station/InyoCounty.mat');

% For comparison between mean and variance (Figure 7)
variance_versus_mean;

% Variance Step Changes
% Table of original rainfall distributions

%generate_station_table; % Table 2

%variance_50ka_samples_new;
% 
% variance_step_change;
