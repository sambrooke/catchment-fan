addpath('../');

run_names = fieldnames(glacial_sim_rainfall);

for nnn = 1:length(run_names)
    run_title = run_names{nnn};
    alpha = glacial_sim_rainfall.(run_title).t002;
    alpha_incident = glacial_sim_rainfall.(run_title).incident;
    alpha_variance = glacial_sim_rainfall.(run_title).variance;
    alpha_events_prop = glacial_sim_rainfall.(run_title).events_prop;
    [kappa,c,nexp,lx,u0,alpha_realstep,C1,ss_yield] = reset_params;
    lx = 3000;
    nexp = 1;
    c = 0.01;
    kappa = 0.005;
    u0 = 0.002;
    C1 = 0.7;
    tmax = length(alpha);
    ss_alpha_condition = alpha(1);
    fan_evo;
    plot_len = length(alpha);
    save(['../data/glacial_sims/' filesep run_title '.mat']);
end
