function alpha = lognormal_mean_conserve(len,val,pd,chance)
    alpha_dist = zeros(len,1);
    
    tolerance = 0.0001;
    inc = 0.00001;
    
    for y=1:len
        
        precip = zeros(365,1);

        for d=1:length(precip)
            if rand <= chance
                precip(d) = random(pd);
            end
        end

        alpha_dist(y) = mean(precip);
    end

    mv = mean(alpha_dist);
    
    if val < mv
        inc = -1*inc;
    end

    stop = 0;

    while stop < 1
        alpha_dist = alpha_dist+inc;
        alpha_dist(alpha_dist<0) = 1e-6;
        mv = mean(alpha_dist);
        if abs(val-mv) < tolerance
            stop = 1;
        end
    end
    
%     c_area = cumtrapz(alpha_dist); % Cumulative area
%     
%     if find(c_area>limit,1)
%        alpha =  alpha_dist(1:find(c_area>limit,1));
%     else
%        alpha = false;
%     end

    alpha =  alpha_dist;
end    