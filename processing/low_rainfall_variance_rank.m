addpath('../processing');
addpath('../data');

% A script to assess the daily variance of each weather station at low
% rainfall levels

load('../data/InyoCounty.mat');

inyo_names = unique(InyoCounty.NAME);
inyo_names = inyo_names(strcmp(inyo_names,'NAME') < 1);

daily_station_variance = zeros(1, length(inyo_names));

for inyo_n = 1:length(inyo_names)
    % Generate Distribution
    station_name = inyo_names{inyo_n};
    disp(station_name);
    rain = InyoCounty(InyoCounty.NAME==station_name,:);
    no_negs = rain.PRCP>-1;
    precip = rain.PRCP(no_negs)/1000; % Convert to metres/day
    daily_station_variance(inyo_n) = std(precip)^2;
    dates = rain.DATE(no_negs);
    dates = datetime(dates, 'ConvertFrom','yyyymmdd');
    no_precip = precip(precip<=0);
    precip = precip(precip>0);
    pd = fitdist(precip, 'lognormal');
    chance = length(precip)/length(no_precip);
    
    years = 10; % Timestep
    
    run_alpha = repmat(0.05, 1, 10);
    [variance, incident, effective, events_prop] = rainfall(10, ...
        run_alpha, pd, 1, chance, 0.05, 0.5);
    
    daily_station_variance(inyo_n) = mean(variance);
    
end

[sorted_variance,I] = sort(daily_station_variance);
sorted_names = inyo_names(I);

save('../data/variance_ranked.mat', 'sorted_names', 'sorted_variance');

