

function [variance, incident, effective, events_prop] = rainfall(timestep, ...
    run_alpha, pds, pd_choices, chances, infiltration_depths, recharge_exponent)
   
   % timestep = years over which to average rainfall (e.g. 100, 1000, 10000 yrs)
   % run_alpha = vector of target mean rainfall
   % pds = database of available incident rainfall distributions
   % pd_choices = indexes of which rainfall dist (pds) to use at each
   % timestep
   % chances = 0 to 1 probabilities of rainfall occuring at each timestep
   
   years = timestep; % Timestep
   
   incident_mean = zeros(1,length(run_alpha)); % Incident rainfall mean
   
   threshold_ids = struct(); % String IDs for each of the inputted thresholds
   % e.g. 0.5 becomes t0_5 or 1 becomes t1
   
   effective_rainfall = struct(); % Where we're going to store all calculated effective rainfall
   for t = 1:length(infiltration_depths)
       depth_str = strrep(num2str(infiltration_depths(t)), '.', '_');
       threshold_ids.(['t', depth_str]) = infiltration_depths(t);
       effective_rainfall.(['t', depth_str]) = zeros(1,length(run_alpha));
   end

   events_prop_list = zeros(1,length(run_alpha)); % Average proportion of effective events per year 
   variance_ky = zeros(length(run_alpha),1);

   for ky = 1:length(run_alpha)

       timestep_rainfall = [];
       
       if length(pds) > 1
           if iscell(pds)
            pd = pds{pd_choices(ky)};
           else
            pd = pds(pd_choices(ky));
           end
           chance = chances(pd_choices(ky));
       else
           pd = pds(1);
           chance = chances(1);
       end
       
        for y=1:years
            precip = zeros(365,1);

            for d=1:length(precip)
                if rand <= chance
                    precip(d) = random(pd);
                end
            end

            timestep_rainfall = [timestep_rainfall; precip];
        end

        mean_target = run_alpha(ky);
        alpha_dist_norm = timestep_rainfall/max(timestep_rainfall);
        mean_orig = mean(alpha_dist_norm);
        alpha_mean_con = alpha_dist_norm*(mean_target/mean_orig);

        % New mean-conserving PDF
        no_precip = alpha_mean_con(alpha_mean_con<=0);
        precip = alpha_mean_con(alpha_mean_con>0);
        pd = fitdist(precip, 'lognormal');
        chance = length(precip)/length(no_precip);

        incident = zeros(years,1);
        events_prop = zeros(years,1);
        
        % Create struct to contain effective rainfall for as many
        % thresholds that have been inputed over the timesteps
        effective_t = struct(); % Where we're going to store all calculated effective rainfall
        for t = 1:length(infiltration_depths)
            depth_str = strrep(num2str(infiltration_depths(t)), '.', '_');
        	effective_t.(['t', depth_str]) = zeros(years,1);
        end

        for y=1:years % Every 1000 years

            precip_empty = 1;

            while precip_empty 
                precip = zeros(365,1);

                for d=1:length(precip)
                    if rand <= chance
                        precip(d) = random(pd);
                    end
                end

                precip(isnan(precip)) = 0;
                if trapz(precip) > 0
                    precip_empty = 0;
                end
            end
            
            % Second enforcement of mean rainfall target
            mean_target = run_alpha(ky);
            precip_norm = precip/max(precip);
            mean_orig = mean(precip_norm);
            precip_mean_con = precip_norm*(mean_target/mean_orig);
            
            variance_ky(ky) = std(precip_mean_con)^2;
            events_prop(y) = (sum(precip_mean_con>0)/365); % Proportion of days per year where rainfall occurs
            incident(y) = mean(precip_mean_con);
            
            for t = 1:length(infiltration_depths)
                depth_str = strrep(num2str(infiltration_depths(t)), '.', '_');
                eF_inf_1 = infiltration(precip_mean_con, infiltration_depths(t), recharge_exponent);
                effective_t.(['t', depth_str])(y) = mean(eF_inf_1(~isnan(eF_inf_1)));
            end

        end
        
        incident_mean(ky) = mean(incident); % Incident rainfall mean
        
        for t = 1:length(infiltration_depths)
            depth_str = strrep(num2str(infiltration_depths(t)), '.', '_');
            effective_rainfall.(['t', depth_str])(ky) = mean(effective_t.(['t', depth_str]));
        end
        
        events_prop_list(ky) = mean(events_prop); % Average proportion of days where rainfall occurs
   end
   
   variance = variance_ky;
   incident = incident_mean;
   effective = effective_rainfall;
   events_prop = events_prop_list;
end
