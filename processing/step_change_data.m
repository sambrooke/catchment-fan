sat_levels = [0, 0.2, 0.5, 1];

step_change_runs = subdir('./data/step_changes/*.mat');

run_length_all = length(step_change_runs);
a1s = zeros(run_length_all,1);
a2s = zeros(run_length_all,1);
v1s = zeros(run_length_all,1);
v2s = zeros(run_length_all,1);
vjumps = zeros(run_length_all,1);
dists_20 = zeros(run_length_all,1);
dists_30 = zeros(run_length_all,1);
dists_40 = zeros(run_length_all,1);
stds_20 = zeros(run_length_all,1);
stds_30 = zeros(run_length_all,1);
stds_40 = zeros(run_length_all,1);
sats = zeros(run_length_all,1);

marker_gs = [40, 30, 20];

for rt = 1:run_length_all
    %try

        load(step_change_runs(rt).name);
        rn = rt;

        range = regexp(step_change_runs(rt).name,'[0-9]+_to_[0-9]+', 'match');
        range = split(range{1}, '_to_');
        a1 = str2double(range{1});
        a2 = str2double(range{2});

        fan_heights = fan_elevation;
        fan_x = coord_basin*lx;

        sg = size(fan_elevation);

        t_end = sg(2)-1;
        x_end = sg(1);

        t_start = 1;

        x_coords_all = [];
        y_coords_all = [];
        indexes = [];

        for mgs = 1:length(marker_gs)
            x_coords = [];
            y_coords = [];

            for y=ss_end+1:1:t_end
                [c index] = min(abs(grainpdf_mm(y,:)-marker_gs(mgs)));
                indexes = [indexes; index];

                x_coords = [x_coords; fan_x(index)];
                y_coords = [y_coords; fan_heights(index, y)];
            end

            x_coords_all = [x_coords_all, x_coords];
            y_coords_all = [y_coords_all, y_coords];
        end
        
        variance1 = variance_alpha(1);
        variance2 = variance_alpha(2);
        
        vjump = variance2-variance1;
 
        dist_40_a1 = mean(x_coords_all(1:50, 1));
        dist_40_a2 = mean(x_coords_all(51:end, 1));
        std_40_a2 =  std(x_coords_all(50:end, 1));
        dist_40 = dist_40_a2-dist_40_a1;

        dist_30_a1 = mean(x_coords_all(1:50, 2));
        dist_30_a2 =  mean(x_coords_all(51:end, 2));
        std_30_a2 =  std(x_coords_all(50:end, 2));
        dist_30 = dist_30_a2-dist_30_a1;

        dist_20_a1 = mean(x_coords_all(1:50, 3));
        dist_20_a2 =  mean(x_coords_all(51:end, 3));
        std_20_a2 =  std(x_coords_all(50:end, 3));
        dist_20 = dist_20_a2-dist_20_a1;

        a1s(rn) = a1;
        a2s(rn) = a2;
        v1s(rn) = variance1;
        v2s(rn) = variance2;
        vjumps(rn) = vjump;
        dists_20(rn) = dist_20;
        dists_30(rn) = dist_30;
        dists_40(rn) = dist_40;
        stds_20(rn) = std_20_a2;
        stds_30(rn) = std_30_a2;
        stds_40(rn) = std_40_a2;
        sats(rn) = sat_levels(sat);
        disp(rn);
    %catch
    %  disp('Did not load that last file');
    %  disp(step_change_runs(rt).name);
    %  disp('-- probably corrupt');
    %end
end

step_data = table(a1s, a2s, v1s, v2s, vjumps, dists_20, dists_30, ...
    dists_40, stds_20, stds_30, stds_40, sats);
writetable(step_data, './data/step_data.csv');
