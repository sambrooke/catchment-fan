% inyo_times_series_variance - ka mean annual variance
% daily_station_variance - input variance per ka
% inyo_time_series_incident - 50ka incident rainfall for 0.1m/yr
% inyo_time_series_t0_2 - 50ka effective for 0_2m depth
% inyo_time_series_t0_5 - 50ka effective for 0_5m depth
% inyo_time_series_t1 - 50ka effective for 1m depth
% inyo_times_series_stations - Weather station names

saturations = {'s0','s0_02','s0_05','s01'};

variance_increase = nchoosek(1:31,2);
variance_decrease = fliplr(variance_increase);
variance_all = [variance_increase; variance_decrease];
vs = size(variance_all);

for vss = 1:564
    
    vars = variance_all(vss,:);
    a1 = vars(1);
    a2 = vars(2);
    
    for sat = 1:length(saturations)
        
        if sat == 1
            alpha_A = inyo_time_series_incident(a1,:);
            alpha_B = inyo_time_series_incident(a2,:);
        elseif sat == 2
            alpha_A = inyo_time_series_t0_02(a1,:);
            alpha_B = inyo_time_series_t0_02(a2,:);
        elseif sat == 3
            alpha_A = inyo_time_series_t0_05(a1,:);
            alpha_B = inyo_time_series_t0_05(a2,:);
        elseif sat == 4
            alpha_A = inyo_time_series_t01(a1,:);
            alpha_B = inyo_time_series_t01(a2,:);
        end
        
        station_A = inyo_times_series_stations{a1};
        station_B = inyo_times_series_stations{a2};
        variance_A = inyo_times_series_variance(a1);
        variance_B = inyo_times_series_variance(a2);
        incident_A = inyo_time_series_incident(a1,:);
        incident_B = inyo_time_series_incident(a2,:);
        event_A = inyo_time_series_event_props(a1,:);
        event_B = inyo_time_series_event_props(a2,:);
        
        [kappa,c,nexp,lx,u0,alpha_realstep,C1,ss_yield] = reset_params;
        kappa = 0.005;
        u0 = 0.001;
        c = 0.01;
        tmax = 2000;
        
        alpha = [alpha_A, alpha_B];
        incident_alpha = [incident_A, incident_B];
        variance_alpha = [variance_A, variance_B];
        variance_change = mean(variance_A)-mean(variance_B);
        alpha_events_prop = [event_A, event_B];
        ss_alpha_condition = mean(incident_alpha);
        
        discrete = 1; % Don't show any waitbars
        fan_evo;
        
        grainpdf_mm = exp(grainpdf');
        
        a1_str = num2str(a1);
        a2_str = num2str(a2);
        
        save(['./data/step_changes' filesep a1_str '_to_' a2_str '_' saturations{sat} '.mat'], ...
            'grainpdf_mm', 'fan_elevation', 'coord_basin', 'lx', 'ss_end',...
            'incident_alpha', 'variance_alpha', 'sat', 'erodq', 'kappa', 'c',...
            'nexp', 'ss_realstep', 'alpha_realstep', 'mean_gradients_fan',...
            'mean_gradients_catchment', 'D50s', 'D84s', 'variance_change',...
            'station_A', 'station_B', 'alpha_events_prop', 'depths', 'tbs');

        clear yield A alpha alpha1 alpha2 alpha_dt alpha_duration ...
           alpha_realstep alpha_start alpha_t alphabar B c C1 C2 ...
           c_gradient Ce Cmult complete_alpha coord_basin ...
           coord_basin_dx coord_catchment coord_catchment_dx ...
           coord_system Cv d D D0 D50 D50s D84 D84s De depo ...
           depoarea depoq depstart differ difference differences ...
           disp_mat displ displ_old dN dt dt_system_tects ek erod ...
           erodq excess f F fan_current fan_current_dimensional ...
           fan_elevation Fbc Free fs full_duration grain_evo ...
           grainpdf grainpdf0 gs1 gs2 h iel iterate j k K ...
           kappa L lambda lx M mean_gradients_catchment midx movement nel ...
           mean_gradients_fan nexp ni nip nn nod ns output_subdir phi0 qsfine ...
           rightbcval sb0 slope ss ss_alpha_condition ss_dt ...
           ss_end ss_message ss_realstep ss_threshold ss_yield ...
           start_diag subs system_tects system_tects_interp t ...
           t_finish tdepo tdiff tdispl tect_mat terod topo ...
           topo_evo tuplif U u0 up up0 uplif upn w y depths tbs
           
    end
end

