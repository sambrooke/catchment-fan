clear;
clc;
addpath('../');
addpath('../processing');
addpath('../plotting');

vb = 1; % Background choice
vc = 20; % Step change

load('../data/inyo_time_series.mat');

run_alphas = [[repmat(inyo_time_series_incident(vb,:),1,10) repmat(inyo_time_series_incident(vb,:),1,10) repmat(inyo_time_series_incident(vb,:),1,10)];...
    [repmat(inyo_time_series_t0_5(vb,:),1,10) repmat(inyo_time_series_t0_5(vc,:),1,10) repmat(inyo_time_series_t0_5(vb,:),1,10)];...
    [repmat(inyo_time_series_t1(vb,:),1,10) repmat(inyo_time_series_t1(vc,:),1,10) repmat(inyo_time_series_t1(vb,:),1,10)]];

events = [repmat(inyo_time_series_event_props(vb,:),1,10) repmat(inyo_time_series_event_props(vc,:),1,10) repmat(inyo_time_series_event_props(vb,:),1,10)];

run_names = {'threshold_incident', 'threshold_t0_5', 'threshold_t1'};

for rn = 1:3
   
   alpha_incident = run_alphas(1,:);
   alpha = run_alphas(rn,:);
   alpha_events_prop = events;
   
   [kappa,c,nexp,lx,u0,alpha_realstep,C1,ss_yield] = reset_params;
   
   kappa = 0.005;
   ss_alpha_condition = mean(alpha(1:200));
   tmax = length(alpha)+100;
   discrete = 1;
   fan_evo;
   plot_len = 1500;
   save(['../data/threshold_runs/' filesep run_names{rn} '.mat']);      
end
