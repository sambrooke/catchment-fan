% Produce 50ka samples for each distribution for a fixed 0.1m/yr mean

load('../weather_station/InyoCounty.mat');

inyo_names = unique(InyoCounty.NAME);
inyo_names = inyo_names(strcmp(inyo_names,'NAME') < 1);

inyo_times_series_variance = zeros(length(inyo_names),50);
daily_station_variance = zeros(1, length(inyo_names));
inyo_time_series_incident = zeros(length(inyo_names),50);
inyo_time_series_t1 = zeros(length(inyo_names),50);
inyo_time_series_t2 = zeros(length(inyo_names),50);
inyo_time_series_t3 = zeros(length(inyo_names),50);
inyo_times_series_stations = inyo_names; % Hold station names

for inyo_n = 1:length(inyo_names)
    % Generate Distribution
    station_name = inyo_names{inyo_n};
    disp(station_name);
    rain = InyoCounty(InyoCounty.NAME==station_name,:);
    no_negs = rain.PRCP>-1;
    precip = rain.PRCP(no_negs)/1000; % Convert to metres/day
    daily_station_variance(inyo_n) = std(precip)^2;
    dates = rain.DATE(no_negs);
    dates = datetime(dates, 'ConvertFrom','yyyymmdd');
    no_precip = precip(precip<=0);
    precip = precip(precip>0);
    pd = fitdist(precip, 'lognormal');
    chance = length(precip)/length(no_precip);
    
    
    years = 1000; % Timestep
    
    incident_ka_variance = zeros(1,50); % ka variance
    incident_mean = zeros(1,50); % Incident rainfall mean
    eF_list_t1 = zeros(1,50); % 1m threshold
    eF_list_t2 = zeros(1,50); % 3m threshold
    eF_list_t3 = zeros(1,50); % 5m threshold
    
    alpha_dist = zeros(years,1);
    
    for ky = 1:50
        for y=1:years
            precip = zeros(365,1);

            for d=1:length(precip)
                if rand <= chance
                    precip(d) = random(pd);
                end
            end

            alpha_dist(y) = mean(precip);
        end
   
        mean_target = 0.05;
        alpha_dist_norm = alpha_dist/max(alpha_dist);
        mean_orig = mean(alpha_dist_norm);
        alpha_mean_con = alpha_dist_norm*(mean_target/mean_orig);

        incident_ka_variance(ky) = std(alpha_mean_con)^2;
        
        incident = zeros(years,1);
        eF_t1 = zeros(years,1);
        eF_t2 = zeros(years,1);
        eF_t3 = zeros(years,1);
        
        for y=1:years

            precip = zeros(365,1);

            for d=1:length(precip)
                if rand <= chance
                    precip(d) = random(pd);
                end
            end

            mean_target = alpha_mean_con(y);
            precip_norm = precip/max(precip);
            mean_orig = mean(precip_norm);
            precip_mean_con = precip_norm*(mean_target/mean_orig);
            
            incident(y) = mean(precip_mean_con);
            eF_list_1 = infiltration(precip_mean_con, 0.02, 0.5);
            eF_list_2 = infiltration(precip_mean_con, 0.05, 0.5);
            eF_list_3 = infiltration(precip_mean_con, 0.1, 0.5);
            eF_t1(y) = mean(eF_list_1);
            eF_t2(y) = mean(eF_list_2);
            eF_t3(y) = mean(eF_list_3);
        end
        
        incident_mean(ky) = mean(incident); % Incident rainfall mean
        
        eF_list_t1(ky) = mean(eF_t1(~isnan(eF_t1))); % 0.5m threshold
        eF_list_t2(ky) = mean(eF_t2(~isnan(eF_t2))); % 1m threshold
        eF_list_t3(ky) = mean(eF_t3(~isnan(eF_t3))); % 2m threshold
        disp(ky);
    end
    
    inyo_times_series_variance(inyo_n,:) = incident_ka_variance;
    inyo_time_series_incident(inyo_n,:) = incident_mean;
    inyo_time_series_t1(inyo_n,:) = eF_list_t1;
    inyo_time_series_t2(inyo_n,:) = eF_list_t2;
    inyo_time_series_t3(inyo_n,:) = eF_list_t3;
end

save('inyo_time_series.mat', 'daily_station_variance', 'inyo_time_series_t1', 'inyo_time_series_t2',...
    'inyo_time_series_t3', 'inyo_time_series_incident', 'inyo_times_series_stations',...
    'inyo_times_series_variance');
