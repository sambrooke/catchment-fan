function alpha = lognormal_integral_conserve(len,limit,pd,chance )
    alpha_dist = zeros(len,1);
    
    tolerance = 0.01;
    
    for y=1:len
        
        precip = zeros(365,1);

        for d=1:length(precip)
            if rand <= chance
                precip(d) = random(pd);
            end
        end

        alpha_dist(y) = mean(precip);
    end
    
    iterate = 1;
    area = trapz(alpha_dist);
        
    while iterate
        
       area = trapz(alpha_dist);
       
       if area < limit
           mode = 1; % We need to ad;
       else
           mode = -1; % We need to substract
       end
       
       difference = abs(area-limit);
       
       if mode < 1
           inc = 0.00001;
       else
           inc = difference/limit;
       end
       
       inc = mode*inc;
       alpha_dist = alpha_dist+inc;
       
       alpha_dist(alpha_dist<0) = 1e-6;
       area = trapz(alpha_dist);
       
       if abs(area-limit) < tolerance
            iterate = 0;
 %      else
 %           disp(abs(area-limit));
       end
       
    end

    alpha =  alpha_dist;
end    