clear;
clc;
addpath('../');
dh_table = load_devils_hole;

vq = interp1(dh_table.dh_ka,dh_table.dh_o18,1:1:100);
vq = 1./vq;
alpha = vq./(max(vq)).*0.13;

min_p = 0.1; % Holocene
max_p = 0.13; % LGM

alpha = ((alpha -min(alpha))./(max(alpha)-min(alpha)))*(max_p-min_p)+min_p;

alpha(isnan(alpha)) = alpha(end);
%alpha = fliplr(alpha);

figure
plot(dh_table.dh_ka,dh_table.dh_o18, 'Linewidth', 1);
hold on;
ylim([13 15]);
ylabel('\delta18O ppm');
yyaxis right
plot(alpha, 'Linewidth', 1);
set(gca,'xdir','reverse');
xlim([0, 100]);
ylim([.05 .2]);
xlabel('ka before present');
ylabel('Mean Annual Rainfall (m)');
grid on
legend('O18 isotopes ppm', 'Rainfall (m)', 'Location', 'north');

% Time lines
