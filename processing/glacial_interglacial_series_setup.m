load('../weather_station/InyoCounty.mat');

% Generate the time series for the glacial interglacial runs
inyo_names = unique(InyoCounty.NAME);
inyo_names = inyo_names(strcmp(inyo_names,'NAME') < 1);

dh_table = load_devils_hole;
vq = interp1(dh_table.dh_ka,dh_table.dh_o18,1:1:150);
vq(isnan(vq)) = 14.5929;
vq = (1./vq);
vq = (vq-min(vq))./max(vq);
vq = vq(5:end)*3;
vq(13) = 0.05;
vq(12) = 0.04;
vq(11) = 0.03;
vq(10) = 0.02;
vq(9) = 0.02;
vq(8) = 0.02;
vq(7) = 0.01;
vq(6) = 0.01;
vq(5) = 0.02;
vq(4) = 0.03;
vq(3) = 0.04;
vq(2) = 0.05;
vq(1) = 0.05;
alpha = (smooth(smooth(vq))*3)+0.3;

alpha = fliplr(alpha');
alpha = interp(alpha,10)/10;

filename = '../data/inyo_stations.csv';
opts = delimitedTextImportOptions("NumVariables", 2);

% Specify range and delimiter
opts.DataLines = [1, Inf];
opts.Delimiter = ",";
% Specify column names and types
opts.VariableNames = ["dh_ka", "dh_ol8"];
opts.VariableTypes = ["double", "double"];
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";
% Import the data
devilsholeo18 = readtable(filename, opts);

opts = delimitedTextImportOptions("NumVariables", 10);
% Specify range and delimiter
opts.DataLines = [2, Inf];
opts.Delimiter = ",";
% Specify column names and types
opts.VariableNames = ["id", "stations", "latitude", "longitude", "elevation", "mean_rainfall", "variance", "mu", "sigma", "chance_list"];
opts.VariableTypes = ["double", "string", "double", "double", "double", "double", "double", "double", "double", "double"];
opts = setvaropts(opts, 2, "WhitespaceRule", "preserve");
opts = setvaropts(opts, 2, "EmptyFieldRule", "auto");
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";
% Import the data
inyostations = readtable("../data/inyo_stations.csv", opts);

run_titles = {'moderate_variance','low_to_high_variance'};

glacial_sim_rainfall = struct();

base_struct = struct('incident', zeros(1,length(alpha)),'t02', zeros(1,length(alpha)),...
    't05', zeros(1,length(alpha)),'t1', zeros(1,length(alpha)));

load('../data/variance_ranked.mat');

% Limit stations to create smooth variance gradient
% dists_sorted_limited = [sorted_names(1:7);sorted_names(25:30)];
% variance_sorted_limited = [sorted_variance(1:7),sorted_variance(25:30)];
dists_sorted_limited = sorted_names;
variance_sorted_limited = sorted_variance;
dists = dists_sorted_limited;

pds = cell(1, length(dists));
chances = zeros(1, length(dists));
pd_variances = zeros(1, length(dists));

for inyo_n = 1:length(dists)
    % Generate Distribution
    station_name = dists{inyo_n};
    rain = InyoCounty(InyoCounty.NAME==station_name,:);
    no_negs = rain.PRCP>-1;
    precip = rain.PRCP(no_negs)/1000; % Convert to metres/day
    no_precip = precip(precip<=0);
    precip = precip(precip>0);
    pd = fitdist(precip, 'lognormal');
    pds{inyo_n} = pd;
    chance = length(precip)/length(no_precip);
    chances(inyo_n) = chance;
    pd_variances(inyo_n) = variance_sorted_limited(inyo_n);
end

max_inf = 1; % Holocene
min_inf = length(dists); % LGM

pd_choices = (alpha)./(max(alpha)).*max_inf;
pd_choices = ((pd_choices -max(pd_choices))./(max(pd_choices)-min(pd_choices)))*(max_inf-min_inf)+max_inf;
pd_choices(isnan(pd_choices)) = max(pd_choices);
pd_choices = fliplr(pd_choices);

pd_choices = ceil(pd_choices);

pd_choices_inverse = (1./alpha)./(max(alpha)).*max_inf;
pd_choices_inverse = ((pd_choices_inverse -max(pd_choices_inverse))./(max(pd_choices_inverse)-min(pd_choices_inverse)))*(max_inf-min_inf)+max_inf;
pd_choices_inverse(isnan(pd_choices_inverse)) = max(pd_choices_inverse);
pd_choices_inverse = fliplr(pd_choices_inverse);
pd_choices_inverse = ceil(pd_choices_inverse);

cycles = 5;
t_len = length(alpha); % Cycles every 100kyr

% Cyclical
% Change from LGM to Holocene 
t_half = t_len/2;
t_cycle = (t_len/(t_half*t_len))*cycles;


shift_t = 1:1:t_len;
pd_choices = floor(6*sin(shift_t.*pi.*t_cycle)+7);


for rt = 1:length(run_titles)
    glacial_sim_rainfall.(run_titles{rt}) = base_struct;
end

for nnn = 1:length(run_titles)
    
    run_name = run_titles{nnn};
    disp(run_name);
    
    years = 100;
    
    if nnn == 1
        % Low variance
        % Variable mean
        
        pd_list = ones(1,length(alpha))*6;
        run_alpha = alpha;
        pd_variances_list = pd_variances(pd_list);
        
        [variance, incident, effective, events_prop] = rainfall(100, run_alpha, ...
            pds, pd_list, chances, [0.02 0.05 0.1], 0.5);
        
    elseif nnn == 2
        
        % Variable variance - low in Interglacials
        % Variable mean
        pd_list = pd_choices;
        run_alpha = alpha;
        pd_variances_list = pd_variances(pd_list);
        
        [variance, incident, effective, events_prop] = rainfall(100, run_alpha, ...
            pds, pd_list, chances, [0.02 0.05 0.1], 0.5);
        
    end
    
    
    % Using a 0.5 threshold so our threshold key is t0_5
    
    glacial_sim_rainfall.(run_name).variance = variance;
    glacial_sim_rainfall.(run_name).incident = incident;
    glacial_sim_rainfall.(run_name).t002 = effective.('t0_02');
    glacial_sim_rainfall.(run_name).t005 = effective.('t0_05');
    glacial_sim_rainfall.(run_name).t01 = effective.('t0_1');
    glacial_sim_rainfall.(run_name).events_prop = events_prop;
end

save('../data/glacial_sim_rainfall.mat', 'glacial_sim_rainfall');
