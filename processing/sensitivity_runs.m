% Workflow for producing specific model runs for sensitivity analysis

% basic setup
[kappa,c,nexp,lx,u0,alpha_realstep,C1,ss_yield] = reset_params();

% variables to test
alpha_means = [0.01, 0.1, 0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]; % metres per year rainfall
kappas = [0.001, 0.002, 0.003, 0.004 0.005, 0.01, ...
    0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1]; % erosional coefficients

c_list = [0.01, 0.000001];
n_list = [1, 2];

sigmas = []; % Rainfall variances
thresholds = []; % Rainfall thresholds
C1s = [0.1 0.2 0.3 0.4 0.5, 0.6, 0.7, 0.8, 0.9];

output_dir = './data/sensitivity_runs';
% All sensitivity tests must be run to new steady state.


% Alpha runs
output_subdir = 'alpha_runs';
for a=1:length(alpha_means)
    set_params = 1;
    tmax = 1000;
    ss_alpha_condition = alpha_means(a);
    alpha = ones(20,1)*alpha_means(a);
    yield = ones(20,1);
    run_title = ['alpha_run_' num2str(alpha_means(a))];
    fan_evo;
    save([output_dir filesep output_subdir filesep run_title '.mat']);
end

[kappa,c,nexp,lx,u0,alpha_realstep,C1,ss_yield] = reset_params();

% Kappa runs
output_subdir = 'kappa_runs';
for a=1:length(kappas)
    set_params = 1;
    tmax = 1000;
    ss_alpha_condition = 0.5;
    alpha = ones(20,1)*0.5;
    yield = ones(20,1);
    kappa = kappas(a);
    run_title = ['kappa_run_' num2str(kappa)];
    fan_evo;
    save([output_dir filesep output_subdir filesep run_title '.mat']);
end

[kappa,c,nexp,lx,u0,alpha_realstep,C1,ss_yield] = reset_params();

% C1 runs
output_subdir = 'c1_runs';
for a=1:length(C1s)
    set_params = 1;
    tmax = 1000;
    ss_alpha_condition = 0.5;
    alpha = ones(20,1)*0.5;
    yield = ones(20,1);
    C1 = C1s(a);
    run_title = ['C1_run_' num2str(C1s(a))];
    fan_evo;
    save([output_dir filesep output_subdir filesep run_title '.mat']);
end

[kappa,c,nexp,lx,u0,alpha_realstep,C1,ss_yield] = reset_params();

% c and n alpha runs - Recover time
output_subdir = 'cn_runs';
for a=1:length(c_list)
    set_params = 1;    
    c = c_list(a);
    nexp = n_list(a);
    
    alpha_realstep = 10000;
    alpha_start = 0.1;
    alpha_step = ones(1,2000)'*0.5;
    alpha_end = ones(1,8000)'*0.1;

    alpha = [alpha_start; alpha_step; alpha_end];
    tmax = length(alpha);
    yield = ones(length(alpha),1);
    ss_alpha_condition = alpha_start;
    run_title = ['cn_run_' num2str(nexp)];
    fan_evo;
    save([output_dir filesep output_subdir filesep run_title '.mat']);
end

[kappa,c,nexp,lx,u0,alpha_realstep,C1,ss_yield] = reset_params();

% c and n alpha runs - Erodq time
output_subdir = 'cn_alpha_runs';
for u=1:length(alpha_means)
    
    for a=1:length(c_list)
        c = c_list(a);
        nexp = n_list(a);
        run_title = ['cn_run_alpha_' num2str(nexp) '_' ...
            num2str(alpha_means(u))];
        set_params = 1;    

        
        output_subdir_n = [output_subdir ...
            filesep 'n' num2str(nexp)];
        
        alpha_start = 0.1;
        alpha_step = ones(1,1500)';
        alpha_end = ones(1,1500)'*0.1;

        alpha = ones(1,20)*alpha_means(u);
        tmax = 100;
        yield = ones(tmax,1);
        ss_alpha_condition = alpha_means(u);
        fan_evo;
        save([output_dir filesep output_subdir_n filesep run_title '.mat']);
    end
end


% n1 alpha runs
output_subdir = 'n1_alpha_runs';
for u=1:length(alpha_means)
    [kappa,c,nexp,lx,u0,alpha_realstep,C1,ss_yield] = reset_params();
    
    run_title = ['n1_alpha_runs' num2str(nexp) '_' ...
        num2str(alpha_means(u))];
    set_params = 1;    
    start_duration = 10;
    shift_duration = 200;
    end_duration = 700;
    
    start_val = 0.01;
    end_val = 0.01;
    ss_alpha_condition = 0.01;
    cycle_amp = alpha_means(u);
    cycles = 0.5;
    t_half = shift_duration/2;
    t_cycle = (shift_duration/(t_half*shift_duration))*cycles;

    shift_t = 1:1:shift_duration;
    start_precip = zeros(1, start_duration)'+start_val;
    shift_precip = cycle_amp*sin(shift_t.*pi.*t_cycle)+start_val;
    end_precip = zeros(1, end_duration)'+end_val;

    alpha = [start_precip; shift_precip'; end_precip];
    tmax = length(alpha);
    yield = ones(tmax,1);
    ss_alpha_condition = start_val;
    fan_evo;
    save([output_dir filesep output_subdir filesep run_title '.mat']);
end
