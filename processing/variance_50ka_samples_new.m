% Produce 50ka samples for each distribution for a fixed 0.1m/yr mean
load('../weather_station/InyoCounty.mat');

inyo_names = unique(InyoCounty.NAME);
inyo_names = inyo_names(strcmp(inyo_names,'NAME') < 1);
inyo_times_series_variance = zeros(length(inyo_names),50);
inyo_time_series_incident = zeros(length(inyo_names),50);
inyo_time_series_t0_02 = zeros(length(inyo_names),50);
inyo_time_series_t0_05 = zeros(length(inyo_names),50);
inyo_time_series_t0_1 = zeros(length(inyo_names),50);
inyo_time_series_incident = zeros(length(inyo_names),50);
inyo_times_series_stations = inyo_names; % Hold station names

parfor inyo_n = 1:length(inyo_names)
    
    % Generate Distribution
    station_name = inyo_names{inyo_n};
    disp(station_name);
    rain = InyoCounty(InyoCounty.NAME==station_name,:);
    no_negs = rain.PRCP>-1;
    precip = rain.PRCP(no_negs)/1000; % Convert to metres/day
    dates = rain.DATE(no_negs);
    dates = datetime(dates, 'ConvertFrom','yyyymmdd');
    no_precip = precip(precip<=0);
    precip = precip(precip>0);
    pd = fitdist(precip, 'lognormal');
    chance = length(precip)/length(no_precip);
    years = 1000; % Timestep
    
    mean_target = 0.05;
    run_len = 50; %ka
    
    run_alpha = ones(run_len, 1)*mean_target;
    
    [variance, incident, effective, events_prop] = rainfall(1000, ...
        run_alpha, pd, 1, chance, [0.02 0.05 0.1], 0.5);
    
    inyo_times_series_variance(inyo_n,:) = variance';
    inyo_time_series_incident(inyo_n,:) = incident;
    inyo_time_series_event_props(inyo_n,:) = events_prop;
    inyo_time_series_t0_02(inyo_n,:) = effective.t0_02;
    inyo_time_series_t0_05(inyo_n,:) = effective.t0_05;
    inyo_time_series_t0_1(inyo_n,:) = effective.t0_1;
    
end


save('./data/inyo_time_series.mat','inyo_time_series_t0_02', 'inyo_time_series_t0_05',...
    'inyo_time_series_t0_1', 'inyo_time_series_incident', 'inyo_times_series_stations',...
    'inyo_times_series_variance', 'inyo_time_series_event_props');
