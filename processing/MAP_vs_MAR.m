% Produce 50ka samples for each distribution for a fixed 0.1m/yr mean

load('../weather_station/InyoCounty.mat');

inyo_names = unique(InyoCounty.NAME);
inyo_names = inyo_names(strcmp(inyo_names,'NAME') < 1);

wetter = 'LAKE SABRINA, CA US';
intermediate = 'BISHOP 8.5 WSW, CA US';
arid = 'STOVEPIPE WELLS 1 SW, CA US';

stations = inyo_names;
station_ids = strrep(strrep(strrep(inyo_names, ' ', '_'), ',', ''), '.', '');
% station_ids = {'wetter', 'intermediate', 'arid'};

decade_series_rainfall = struct();
decade_series_runoff = struct();

% This run is to assess the mean annual runoff (MAR) in mm/day
% against the Mean Annual Precipitation (MDP) for 


for inyo_n = 1:length(stations)
    
    % Generate Distribution
    station_name = stations{inyo_n};
    station_id = station_ids{inyo_n};
    disp(station_name);
    rain = InyoCounty(InyoCounty.NAME==station_name,:);
    no_negs = rain.PRCP>-1;
    precip = rain.PRCP(no_negs)/1000; % Convert to metres/day
    daily_station_variance(inyo_n) = std(precip)^2;
    dates = rain.DATE(no_negs);
    dates = datetime(dates, 'ConvertFrom','yyyymmdd');
    no_precip = precip(precip<=0);
    precip = precip(precip>0);
    pd = fitdist(precip, 'lognormal');
    chance = length(precip)/length(no_precip);
    
    MAP = [0.01 0.02 0.03 0.04 0.05];
   	MAP_L = length(MAP);

    years = 500; % Decade run
        
    mean_rainfall = zeros(MAP_L, years);
    d50_rainfall = zeros(MAP_L, years);
    d84_rainfall = zeros(MAP_L, years);
    d99_rainfall = zeros(MAP_L, years);
    events = zeros(MAP_L, years);
    variance = zeros(MAP_L, years);
    max_events = zeros(MAP_L, years);
    mean_runoff = zeros(MAP_L, years);
    d50_runoff = zeros(MAP_L, years);
    d84_runoff = zeros(MAP_L, years);
    d99_runoff = zeros(MAP_L, years);
    precip_examples = zeros(MAP_L, 365);
   
   for mt = 1:MAP_L
       
       pds = pd;
       pd_choices = 1;
       chances = chance;
       years = 10;
       run_alpha = repmat(MAP(mt),1,years);
       infiltration_depths = 0.01;
       recharge_exponent = 0.5;
       
       for ky = 1:length(run_alpha)

           timestep_rainfall = [];

           if length(pds) > 1
               if iscell(pds)
                pd = pds{pd_choices(ky)};
               else
                pd = pds(pd_choices(ky));
               end
               chance = chances(pd_choices(ky));
           else
               pd = pds(1);
               chance = chances(1);
           end
            
            for y=1:years
                precip = zeros(365,1);

                for d=1:length(precip)
                    if rand <= chance
                        precip(d) = random(pd);
                    end
                end

                timestep_rainfall = [timestep_rainfall; precip];
            end

            mean_target = run_alpha(ky);
            alpha_dist_norm = timestep_rainfall/max(timestep_rainfall);
            mean_orig = mean(alpha_dist_norm);
            alpha_mean_con = alpha_dist_norm*(mean_target/mean_orig);

            % New mean-conserving PDF
            no_precip = alpha_mean_con(alpha_mean_con<=0);
            precip = alpha_mean_con(alpha_mean_con>0);
            pd = fitdist(precip, 'lognormal');
            chance = length(precip)/length(no_precip);

            incident = zeros(years,1);
            events_prop = zeros(years,1);

            % Create struct to contain effective rainfall for as many
            % thresholds that have been inputed over the timesteps
            effective_t = struct(); % Where we're going to store all calculated effective rainfall
            for t = 1:length(infiltration_depths)
                depth_str = strrep(num2str(infiltration_depths(t)), '.', '_');
                effective_t.(['t', depth_str]) = zeros(years,1);
            end

            for y=1:years

                precip_empty = 1;

                while precip_empty 
                    precip = zeros(365,1);

                    for d=1:length(precip)
                        if rand <= chance
                            precip(d) = random(pd);
                        end
                    end

                    precip(isnan(precip)) = 0;
                    if trapz(precip) > 0
                        precip_empty = 0;
                    end
                end

                % Second enforcement of mean rainfall target
                mean_target = run_alpha(ky);
                precip_norm = precip/max(precip);
                mean_orig = mean(precip_norm);
                precip_mean_con = precip_norm*(mean_target/mean_orig);

%                 variance_ky(ky) = std(precip_mean_con)^2;
%                 events_prop(y) = (sum(precip_mean_con>0)/365); % Proportion of days per year where rainfall occurs
%                 incident(y) = mean(precip_mean_con);
                precip_examples(mt,:) = precip_mean_con;
                precip_events = precip_mean_con(precip_mean_con>0);
                events(mt, y) = length(precip_events);
                max_events(mt, y) = prctile(precip_events, 99);
                variance(mt, y) = std(precip_events)^2;
                mean_rainfall(mt, y) = mean(precip_mean_con);
                d50_rainfall(mt, y) = prctile(precip_events, 50);
                d84_rainfall(mt, y) = prctile(precip_events, 84);
                d99_rainfall(mt, y) = prctile(precip_events, 99);

                run_off = infiltration(precip_mean_con, 0.5, 0.5);
                run_off_events = run_off(run_off>0);

                mean_runoff(mt, y) = mean(run_off);
                d50_runoff(mt, y) = prctile(run_off_events, 50);

                d84_runoff(mt, y) = prctile(run_off_events, 84);
                d99_runoff(mt, y) = prctile(run_off_events, 99);   

%                 for t = 1:length(infiltration_depths)
%                     depth_str = strrep(num2str(infiltration_depths(t)), '.', '_');
%                     eF_inf_1 = infiltration(precip_mean_con, infiltration_depths(t), recharge_exponent);
%                     effective_t.(['t', depth_str])(y) = mean(eF_inf_1(~isnan(eF_inf_1)));
%                 end

            end

%             incident_mean(ky) = mean(incident); % Incident rainfall mean
% 
%             for t = 1:length(infiltration_depths)
%                 depth_str = strrep(num2str(infiltration_depths(t)), '.', '_');
%                 effective_rainfall.(['t', depth_str])(ky) = mean(effective_t.(['t', depth_str]));
%             end
% 
%             events_prop_list(ky) = mean(events_prop); % Average proportion of days where rainfall occurs
       end
   end
   
    decade_series_rainfall.(station_id) = struct('station', station_name,...
        'mean', mean_rainfall, 'd50', d50_rainfall, 'd84', d84_rainfall,...
        'd99', d99_rainfall, 'events', events, 'max_events', max_events,...
        'variance', variance, 'precip_examples', precip_examples);
    
    decade_series_runoff.(station_id) = struct('station', station_name,...
        'mean', mean_runoff, 'd50', d50_runoff, 'd84', d84_runoff,...
        'd99', d99_runoff, 'events', events, 'max_events', max_events,...
        'variance', variance, 'precip_examples', precip_examples);
end

save('../data/decade_series.mat', 'decade_series_rainfall', 'decade_series_runoff');
