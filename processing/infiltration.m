function eF_list = infiltration(alpha, depth, k_inf)
    
    eF_list = zeros(length(alpha),1);
    sF_list = zeros(length(alpha),1); 
    for al = 1:length(alpha)

        if al == 1
            sF = 0; % Initial subsurface storage is nil
        else
            sF = (sF_list(al-1)*exp(-k_inf));
            % Decay anything left in previous timestep
        end

        total_water = alpha(al)+sF;
        if total_water > depth
            eF = total_water-depth;
            sF = depth;
        else
            eF = 0;
            sF = total_water;
        end
        sF_list(al) = sF;
        eF_list(al) = eF;
    end
    eF_list(isnan(eF_list)) = 0;  
    eF_list(eF_list<0) = 0;  
end