% Table 2 in manuscript

inyo_names = unique(InyoCounty.NAME);
inyo_names = inyo_names(strcmp(inyo_names,'NAME') < 1);
inyo_names = inyo_names(strcmp(inyo_names,'BISHOP 1.6 NW, CA US') < 1);

dists = inyo_names;
latitude = zeros(length(dists),1);
longitude = zeros(length(dists),1);
elevation = zeros(length(dists),1);
mean_rainfall = zeros(length(dists),1);
variance = zeros(length(dists),1);
mu = zeros(length(dists),1);
sigma = zeros(length(dists),1);
chance_list = zeros(length(dists),1);

for s = 1:length(dists)
    fig = figure();

    set(fig,'visible','off');
    set(fig, 'PaperSize',[X Y]);
    set(fig, 'PaperPosition',[0 yMargin xSize ySize])
    set(fig, 'PaperUnits','centimeters');
    station_name = dists{s};
    rain = InyoCounty(strcmp(InyoCounty.NAME, station_name),:);
    no_negs = rain.PRCP>-1;
    precip = rain.PRCP(no_negs); % Convert to metres/day
    no_precip = precip(precip<=0);
    precip = precip(precip>0);
    pd = fitdist(precip, 'lognormal');
    chance = length(precip)/length(no_precip);
    
    years = 1000;
    
    alpha_dist = zeros(years,1);
    variance_ka = zeros(years,1);
    

    latitude(s) = rain.LATITUDE(1);
    longitude(s) = rain.LONGITUDE(1);
    elevation(s) = rain.ELEVATION(1);
    sigma(s) = pd.sigma;
    mu(s) = pd.mu;
    chance_list(s) = chance;

    all_precips = [];
    for y=1:years

        precip = zeros(365,1);

        for d=1:length(precip)
            if rand <= chance
                precip(d) = random(pd);
            end
        end

        all_precips = [all_precips; precip];
    end
    
    mean_rainfall(s) = mean(all_precips);
    variance(s) = std(all_precips)^2;

end

stations = dists;

inyo_stations = table(stations,latitude,longitude,elevation,mean_rainfall,variance, mu, sigma, chance_list);

writetable(inyo_stations,'./data/inyo_stations.csv');
