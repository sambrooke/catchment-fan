station_names = unique(InyoCounty.NAME);

station_data = struct();

for u = 1:length(station_names)
    rows = InyoCounty.NAME==station_names(u);
    station_name = station_names{u};
    station_name = strrep(station_name, '.','_');
    station_name = strrep(station_name, ',','');
    station_data.(char(strrep(station_name, ' ', '_'))) = InyoCounty(rows, {'ELEVATION', 'LATITUDE', 'LONGITUDE', 'DATE', 'PRCP'});
end

station_vars = fieldnames(station_data);

figure
legend_items = [];
legend_labels = {};

for u = 1:length(station_vars)
    if ((strcmp(station_names(u), 'GRANT GROVE CA US') || strcmp(station_names(u), 'WILDROSE R S CA US')) < 1)
        current_station = station_data.(station_vars{u});
        no_negs = current_station.PRCP>-1;
        precip = current_station.PRCP(no_negs);
        dates = current_station.DATE(no_negs);
        dates = datetime(dates, 'ConvertFrom','yyyymmdd');
        w = gausswin(1000); % 10 Day gaussian window
        y = filter(w,1,precip);
        hp = plot(dates, precip, '-');
        legend_items = [legend_items, hp];
        legend_labels = [legend_labels, char(station_names(u))];
        hold on;
    end
end

legend(legend_items, legend_labels);

