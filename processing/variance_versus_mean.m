% Figure 6 data
% Compare the thresholded vs unthresholded

% low variance station - "BISHOP 1.6 NW, CA US"
% high variance station - "LAWS 0.1SW, CA US"

generate_rainfall = 1;

if generate_rainfall > 0
    runs = struct();
    runs.('mean_no_threshold') = struct('cmax', 0, 'type', 'mean_run', 'effective_id', 't0');
    runs.('mean_05m_threshold') = struct('cmax', 0.5, 'type', 'mean_run', 'effective_id', 't0_5');
    runs.('variance_no_threshold') = struct('cmax', 0, 'type', 'variance_run', 'effective_id', 't0');
    runs.('variance_05m_threshold') = struct('cmax', 0.5, 'type', 'variance_run', 'effective_id', 't0_5');

    low_variance_station = 'BISHOP 1.6 NW, CA US'; % Rain nearly every day
    high_variance_station = 'LAWS 0.1SW, CA US'; % Very low frequency rainfall

    mean_rainfall_change = [ones(200,1)*0.1;ones(200,1)*0.4;ones(200,1)*0.1];
    mean_no_change = [ones(200,1)*0.1;ones(200,1)*0.1;ones(200,1)*0.1];

    variable_run = [ones(200,1)*1;ones(200,1)*2;ones(200,1)*1];
    fixed_run = [ones(200,1)*1;ones(200,1)*1;ones(200,1)*1];

    % Low variance PDF
    rain = InyoCounty(InyoCounty.NAME==low_variance_station,:);
    no_negs = rain.PRCP>-1;
    precip = rain.PRCP(no_negs); % Convert to metres/day
    no_precip = precip(precip<=0);
    precip = precip(precip>0);
    pd_low = fitdist(precip, 'lognormal');
    chance_low = length(precip)/length(no_precip);

    % High variance PDF
    rain = InyoCounty(InyoCounty.NAME==high_variance_station,:);
    no_negs = rain.PRCP>-1;
    precip = rain.PRCP(no_negs); % Convert to metres/day
    no_precip = precip(precip<=0);
    precip = precip(precip>0);
    pd_high = fitdist(precip, 'lognormal');
    chance_high = length(precip)/length(no_precip);

    pds = {pd_low, pd_high};
    chances = [chance_low, chance_high];

    run_names = fieldnames(runs);
    
    for rn = 1:4
       rdata = runs.(run_names{rn});

       disp(run_names{rn});

       if strcmp(rdata.type, 'mean_run') > 0
           run_alpha = mean_rainfall_change;
           pd_choices = fixed_run;
       else
           run_alpha = mean_no_change;
           pd_choices = variable_run;
       end

       years = 1000; % Timestep

       [variance, incident, effective, events_prop] = rainfall(1000, run_alpha, ...
            pds, pd_choices, chances, rdata.cmax, 0.5);
    
       runs.(run_names{rn}).incident = incident;
       runs.(run_names{rn}).effective = effective.(rdata.effective_id);
       runs.(run_names{rn}).variance = variance;
       runs.(run_names{rn}).events_prop = events_prop;
    end

    save('./data/variance_mean_alpha.mat', 'runs');
else
   
   % THIS IS REDUNDANT
   % We had separate runs... also no_threshold did actually use a 1m
   % threshold!! So make sure we use the incident rainfall - 26/6/18
   
   load('./data/variance_mean_alpha4'); % Variance 0.5m threshold
   runs_variance05 = runs;
   
   load('./data/variance_mean_alpha');
   
   runs_variance = runs;
   load('./data/variance_mean_alpha5');

   runs.variance_05m_threshold = runs_variance05.variance_05m_threshold;
   runs.variance_no_threshold = runs_variance.variance_no_threshold;
   runs.variance_no_threshold.effective = runs.variance_no_threshold.incident;
   runs.mean_no_threshold.effective = runs.mean_no_threshold.incident;
end


run_names = fieldnames(runs);

for rn = 1:4

   rdata = runs.(run_names{rn});
   
   alpha_incident = rdata.incident;
   alpha_events_prop = rdata.events_prop;
   alpha = rdata.effective;
   
   [kappa,c,nexp,lx,u0,alpha_realstep,C1,ss_yield] = reset_params;
   kappa = 0.005;
   ss_alpha_condition = mean(alpha(1:200));
   tmax = length(alpha)+100;
   
   fan_evo;
   plot_len = 600;
   save(['./data/null_runs/' filesep run_names{rn} '.mat']);      
end


