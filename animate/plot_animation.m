clear all;
clc;
% load('../data/with_variance.mat');
% load('../data/glacial_sim_rainfall2.mat');
% incident_alpha = glacial_sim_rainfall.mean_rainfall_with_variance.incident;
% incident_variance = glacial_sim_rainfall.mean_rainfall_with_variance.variance;
% run_title = 'frames';
% plot_len = 2000;
% plot_destination = './frames1';

load('../data/no_variance.mat');
load('../data/glacial_sim_rainfall.mat');
incident_alpha = glacial_sim_rainfall.mean_rainfall_no_variance.incident;
incident_variance = glacial_sim_rainfall.mean_rainfall_no_variance.variance;
run_title = 'frames';
plot_len = 2000;
plot_destination = './frames';

colormap jet;
X = 40;
Y = 21;                     %# left/right margins from page borders
xMargin = 0;
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# figure size on paper (widht & hieght)
ySize = Y - 2*yMargin;     %# figure size on paper (widht & hieght)
% Start at which timestep
plot_t_start = ss_end+1;
plot_len = ss_end+plot_len;
%elevation_limits = [-160 20];

% X step size
dx = 20;
t_step = 20;
xMargin = 0;
i = 0;

for j=plot_t_start:t_step:plot_len
    i = i+1;

    if i < 10
     frame = '00'+string(i);
    elseif i < 100
     frame = '0'+string(i);
    else
     frame = string(i);
    end

    fig = figure();
    set(fig,'visible','off');
    set(fig, 'PaperSize',[X Y]);
    set(fig, 'PaperPosition',[0 yMargin xSize ySize])
    set(fig, 'PaperUnits','centimeters');

    % Using natural log so we remake grain size with exp()
    gcolour = exp(grainpdf')./max(exp(grainpdf(:)));
    %
    grainpdf_mm = exp(grainpdf');
    max_size = 250;
    min_size = 100;
    extreme_len = ceil((max_size-min_size)/4);
    extreme_len_1 = floor(extreme_len/4)+1;
    extreme_len_2 = floor(extreme_len/4)+1;
    extreme_len_3 = extreme_len-(extreme_len_1+extreme_len_2)+2;

    start_colour = [0.5 0 0];
    intermediate_colour = [0.49 0.18 0.56];
    end_colour = [1 0 1];
    final_colour = [1 0.7 1];

    R_1 = linspace(start_colour(1),intermediate_colour(1), extreme_len_1);
    R_1 = R_1(2:end)';
    R_2 = linspace(intermediate_colour(1),end_colour(1), extreme_len_2);
    R_2 = R_2(2:end)';
    R_3 = linspace(end_colour(1),final_colour(1), extreme_len_3)';

    G_1 = linspace(start_colour(2),intermediate_colour(2), extreme_len_1);
    G_1 = G_1(2:end)';
    G_2 = linspace(intermediate_colour(2),end_colour(2), extreme_len_2);
    G_2 = G_2(2:end)';
    G_3 = linspace(end_colour(2),final_colour(2), extreme_len_3)';

    B_1 = linspace(start_colour(3),intermediate_colour(3), extreme_len_1);
    B_1 = B_1(2:end)';
    B_2 = linspace(intermediate_colour(3),end_colour(3), extreme_len_2);
    B_2 = B_2(2:end)';
    B_3 = linspace(end_colour(3),final_colour(3), extreme_len_3)';

    extreme_cmap = [R_1,G_1,B_1;R_2,G_2,B_2;R_3,G_3,B_3];
    cm = jet((max_size-min_size)-extreme_len);
    cm = [cm;extreme_cmap];
    
    if min_size > 0
        R_1 = linspace(1, 0, min_size);
        R_1 = R_1(2:end)';

        G_1 = linspace(1, 0, min_size);
        G_1 = G_1(2:end)';

        B_1 = linspace(1, 0.5238, min_size);
        B_1 = B_1(2:end)';
        min_cmap = [R_1,G_1,B_1];
   
        cm = [min_cmap;cm];
    end
    
    colormap(cm);
    % Y step size

    t_start = plot_t_start;


    load(['../data/evo_data/' run_name '_fan_evo_' num2str(j) '.mat']);
    fan_heights = fan_topo_evo;
    sg = size(grainpdf_mm);
    fh = size(fan_heights);
    % t_end = fs_y;
    % x_end = fs_x;
    t_end = fh(2)-1;
    x_end = sg(2);

    fan_x = coord_basin*lx;
    fan_dx = fan_x(x_end)/max(x_end);
    ss_aquisition = plot_t_start;

    alpha_x = 1:1:length(alpha);
    alpha_x = alpha_x.*alpha_realstep./1e3;
    alpha_x = alpha_x((plot_t_start):end);

    maxPrecip = max(alpha);
    maxErosion = max(erodq(t_start:end))+1;

    title('Fan architecture');

    a= annotation('textbox', [0 0.89 1 0.1], ...
        'String', run_title, ...
        'EdgeColor', 'none', ...
        'HorizontalAlignment', 'center');
    set(a,'interpreter','none')
    sp1 = subplot(3,2,1);
    set(sp1,'defaultAxesColorOrder',[[0 0 0]; [0 0 0]]);

    plot(alpha(1:end), 'LineWidth', .5, 'Color', 'b');
    hold on;
    plot(incident_alpha(1:end), 'LineWidth', .5, 'Color', 'k');
    hold on;

    a_diff = j-plot_t_start;
    p1 = plot(alpha(1:a_diff+1), 'LineWidth', 2, 'Color', 'b');
    hold on;
    p2 = plot(incident_alpha(1:a_diff+1), 'LineWidth', 2, 'Color', 'k');
    hold on;
    title('Rainfall');
    ylabel('MAP (m/yr)');
    ylim([0 0.25])
    xticks([0 500 1000 1500 2000]);
    xticklabels({0 50 100 150 200});
    xlabel('ka');
    yyaxis right
    plot(incident_variance(1:end), '-m', 'LineWidth', 0.5);
    hold on;
    p3 = plot(incident_variance(1:a_diff+1), '-m', 'LineWidth', 2);
    yticks(0:0.5:2);
    yticklabels(0:0.5:2);
    ylim([0 10]);
    ylabel('Variance');
    legend([p1 p2 p3],{'Effective Rainfall', 'Incident Rainfall', 'Rainfall Variance'},...
        'Location','north','Orientation','horizontal')
    grid on
    subplot(3,2,2)
    plot(erodq(plot_t_start:end), 'LineWidth', .5, 'Color', 'r')
    hold on;
    plot(erodq(plot_t_start:j+1), 'LineWidth', 2, 'Color', 'r')
    ylim([0 maxErosion+1])
    title('Sediment Flux');
    ylabel('QS (m^2/yr)');
    xticks([0 500 1000 1500 2000]);
    xticklabels({0 50 100 150 200});
    xlabel('ka');
    grid on
    subplot(3,2,[3 6])


    for y=t_start:t_step:t_end

        y_b = y;
        y_t = y+t_step;
        if y_t > t_end
            y_t = t_end;
        end

        for x=1:dx:x_end
            x_l = x;
            x_r = x+dx;

            if x_r > x_end
                x_r = x_end;
            end

            % SWAPPED fan_elevation(t,:) to fan_elevation(:,t)
            xco = [fan_x(x_l) fan_x(x_r) fan_x(x_r) fan_x(x_l)];
            zco = [fan_heights(x, y_t), fan_heights(x_r,y_t), fan_heights(x_r,y_b), fan_heights(x,y_b)];

    %        colourID = mean(mean(grainpdf_mm(y_b:y_t,x_l:x_r),'omitnan'),'omitnan');
           colourID = max(max(grainpdf_mm(y_b:y_t,x_l:x_r)));

            if isnan(colourID)
                colour = [1 1 1];
            else
                color_ceil = ceil(colourID);
                if color_ceil > max_size
                    colour = cm(end,:);
                else
                    colour = cm(color_ceil,:);
                end
            end

            if sum(colour) == 0
               disp('No colour!');
            end
            h = patch(xco, zco, colour, 'EdgeColor', 'none');
            drawnow
        end

    end

    hcb=colorbar;    
    set(hcb,'YTick',  0:.2:1);
    set(hcb,'YTickLabel',linspace(0,ceil(max_size),6));

    ylabel(hcb,'Mean Grain Size (mm)')

    ss_realstep = lx^2/kappa*ss_dt;
    alpha_realstep = lx^2/kappa*alpha_dt;
    real_alpha_max = alpha_realstep * full_duration;
    real_ss_max = ss_realstep*plot_t_start;

    xlabel('Metres');
    ylabel('Metres');
    custom_lines = [t_start, j];


    marker_gs = [210 190 170 150];
%
%     for mgs = 1:length(marker_gs)
%         x_coords = [];
%         y_coords = [];
%         for y=ss_end+1:1:t_end
%             [c index] = min(abs(grainpdf_mm(y,:)-marker_gs(mgs)));
% 
%             x_coords = [x_coords; fan_x(index)];
%             y_coords = [y_coords; fan_heights(index, y)];
%         end
% 
%         line(x_coords, y_coords,'LineWidth',0.5, 'Color', [0.2 0.2 0.2]);
%     end

    t_slice_array = [500 1000 1500];
    
    for kslice = 1:length(t_slice_array)
        if j > t_slice_array(kslice)
            ka_line = fan_heights(:, t_slice_array(kslice));
            line(1:fan_dx:fan_x(x_end), ka_line,'LineWidth',1.5, 'Color', [0 0 0]);
        end
    end
%
    for ts = 1:length(custom_lines)
        cs_line = fan_heights(:, custom_lines(ts));
        line(1:fan_dx:fan_x(x_end), cs_line,'LineWidth',1.3, 'Color', [0 0 0]);
    end
%
%     run_params = {['\bf kappa', '\rm ',  num2str(kappa), '\bf   ss time  ', '\rm ',  [num2str(real_ss_max/1e6) 'Ma']], ...
%         ['\bf n ', '\rm ',  num2str(nexp), '\bf   run time  ', '\rm ',  [num2str(real_alpha_max/1e3) 'kyr']], ...
%         ['\bf c  ', '\rm ',  num2str(c)], ...
%         ['\bf catchment uplift  ', '\rm ',  num2str(u0), 'my^{-1}'], ...
%         ['\bf catchment length  ', '\rm ',  num2str(lx) 'm']
%     };

    title('Fan Architecture');
    %h1 = textLoc(run_params,{'northeast', 1/10}, 'EdgeColor', 'black', 'FontSize', 8);
    print(fig, [plot_destination filesep run_title char(frame) '.png'],'-dpng')

    close(fig);
end
