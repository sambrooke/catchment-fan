% Get dt
function dt = get_dt(kappa, dur, alpha_nn, lx)
    real_timestep = dur / alpha_nn;
    dt = (real_timestep * kappa) / lx^2;
end


