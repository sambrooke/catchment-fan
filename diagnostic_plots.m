addpath('lib');

sims = './data/
addpath('./lib');

to_plot = sims(2);
run_file = to_plot.name;
load(run_file);

figure;
subplot(2,3,2);

plot(alpha_incident);
hold on;
plot(alpha);
legend('Incident', 'Effective', 'Location', 'best');
ylabel('MAP (m/yr)');
title('Rainfall');

subplot(2,3,3);

plot(alpha_variance);
ylabel('\sigma^2');
title('Variance');

subplot(2,3,4);

plot(Qw);
ylabel('Discharge (m^3/yr)');
yyaxis right
plot(tbs);
ylabel('Basal shear stress (N)');
title('Discharge and Shear stress');

subplot(2,3,5);

plot(D50s);
ylabel('Median grain size (mm)');
hold on;
plot(D84s);
ylabel('D84 grain size (mm)');
title('Grain size');

subplot(2,3,6);

plot(erodq);
ylabel('Qs (m^2/ka)');


