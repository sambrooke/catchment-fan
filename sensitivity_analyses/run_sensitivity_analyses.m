% Generates data for figure 5

clc;
clear;

addpath('./lib');
addpath('./model');
addpath('./processing');
addpath('./figures');
addpath('./figures/sensitivity_analyses');
addpath('./lib/topotoolbox');
addpath('./lib/topotoolbox/tools_and_more');
    
load('./data/inyo_time_series.mat');
load('./data/brooke2018grainsize.mat');
%sensitivity_runs;

grape_dem = GRIDobj('./data/dems/grapevine_dem3.tif');
grape_dem = inpaintnans(grape_dem);

grotto_dem = GRIDobj('./data/dems/grotto_dem8.tif');
grotto_dem = inpaintnans(grotto_dem);

santa_rosa_dem = GRIDobj('./data/dems/santa_rosa.tif');
santa_rosa_dem = inpaintnans(santa_rosa_dem);

n_exponent_gradients;