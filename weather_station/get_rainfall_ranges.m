% Script to extract the mean rainfall and variance ranges for Inyo County
% weather stations.

% We are going to decouple mean rainfall and variance

load_station_table;


% MAP
min_mean = min(inyostations.mean_rainfall);
max_mean = max(inyostations.mean_rainfall);

mean_range = linspace(round(min_mean, 2), round(max_mean, 2), 4);

% chance
min_chance = min(inyostations.chance_list);
max_chance = max(inyostations.chance_list);


% stdev
min_stdev = min(inyostations.stdevs);
max_stdev = max(inyostations.stdevs);

stdev_range = linspace(round(min_stdev, 2), round(max_stdev, 2), 4);
% 
% disp('Chance range')
% disp(chance_range')
% 
% disp('MAP range')
% disp(mean_range')
% 
% disp('Sigma range')
% disp(stdev_range')

disp(inyostations.stdevs)
% Create distributions