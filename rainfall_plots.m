load('./data/glacial_sim_rainfall.mat');
figure;
subplot(2,3,1)
plot(glacial_sim_rainfall.mean_rainfall_no_variance.variance)

subplot(2,3,2)
plot(glacial_sim_rainfall.mean_rainfall_no_variance.incident)
hold on;
plot(glacial_sim_rainfall.mean_rainfall_no_variance.t05)

subplot(2,3,3)
plot(glacial_sim_rainfall.mean_rainfall_no_variance.incident)
hold on;
plot(glacial_sim_rainfall.mean_rainfall_no_variance.t1)

subplot(2,3,4)
plot(glacial_sim_rainfall.mean_rainfall_with_variance.variance)

subplot(2,3,5)
plot(glacial_sim_rainfall.mean_rainfall_with_variance.incident)
hold on;
plot(glacial_sim_rainfall.mean_rainfall_with_variance.t05)

subplot(2,3,6)
plot(glacial_sim_rainfall.mean_rainfall_with_variance.incident)
hold on;
plot(glacial_sim_rainfall.mean_rainfall_with_variance.t1)
