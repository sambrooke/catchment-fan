% 1-D FEM model for catchment-fan evolution

full_duration = length(alpha);

% Show waitbars?
nowaitbars = 0;
if exist('discrete')
    if discrete == 1
        nowaitbars = 1;
    end
end

% Therefore full_duration * alpha_realstep = alpha_duration

alpha_duration = alpha_realstep*full_duration;
alpha_dt = get_dt(kappa, alpha_duration, tmax, lx);

% Define ss real timestep
% e.g. every millions years = 1e6
ss_realstep = 1e6;
ss_dt = get_dt(kappa, ss_realstep, 1, lx);

% geometry
nn = 1000; % Catchment
ns = 1000; % Basin
nel = nn+ns;
h =1/nel;

% numerics
nod = 2;     % number of nodes per element
nip = 2;     % number of integration points

% allocate arrays
displ = zeros(nel,1); % displacement 
tdispl = zeros(nel,tmax); %total displacement
tdepo = zeros(ns,tmax); %total deposition
terod = zeros(nn,tmax); %total erosion
tuplif = zeros(nel,tmax); %total uplift
tdiff = zeros(nel,tmax); %total diff
erodq=zeros(1,tmax); % eroded area
depoq=zeros(1,tmax); % eroded area
depstart=zeros(1,tmax); % node where deposition begins

mean_gradients_fan = zeros(tmax,1);
mean_gradients_catchment = zeros(tmax,1);
Qw = zeros(tmax,1);
depths = zeros(tmax,1);
tbs = zeros(tmax,1);
D50s = zeros(tmax,1);
D84s = zeros(tmax,1);
D=zeros(nel,1); % picewise diffusion coefficient
up=zeros(nel,1); % uplift by element
upn=zeros(nn,1); % uplift by node
uplif=zeros(nn,1); % uplift without erosion

subs=zeros(ns,1); % subsidence
differ=zeros(ns,1);
topo=zeros(nel,tmax); % topography across the system by timestep

U=zeros(nel,1); % upper triangle matrix
start_diag = sparse(diag(ones(nel+1,1))+diag(ones(nel,1),1)+diag(ones(nel,1),-1));
M = sparse(start_diag(1:nel,1:nel));
K = sparse(start_diag(1:nel,1:nel));
F=zeros(nel,1); % global force vector

Fbc = zeros(nel,1);

% 1-D linear shape functions
dN=[-1 1];

% Dimensionless uplift/subsidence
up0=u0*lx/kappa;
sb0=u0*lx/kappa;

coord_catchment = linspace(0,1,nn);
coord_catchment_dx = coord_catchment.*(lx/ns)*1e3;
coord_basin = linspace(0,1,ns);
coord_basin_dx = coord_basin.*(lx/ns)*1e3;

% Normal expo
alpha1 = 1; % -ln(.5)/.75
alpha2 = 1; % -ln(.5)/.375

upn = flipud(up0*exp((1-coord_catchment))'-up0);
subs = flipud(-sb0*exp(coord_basin)'+sb0);
% Hole in the ground!
%subs = -sb0.*ones(length(coord_basin),1);

system_tects = [upn;subs];
coord_system = linspace(0,1,length(system_tects));

% Displacement field for elements
d = (1:1:length(system_tects))';
f = system_tects;
j = (1:.5:length(system_tects))';
system_tects_interp = interp1q(d,f,j);

% assemble mass and stiffness matrix and force vector
Ce=h*[1/3 1/6;1/6 1/3];
Ce=diag(sum(Ce)); % lumped

rightbcval = 0;

% Iterate
ss_threshold = 0.00000001;
ss = 1;
alpha_start = 0;
iterate = 1;

if nowaitbars == 0
    ss_message = msgbox('Acquiring steady state');
end

alpha_t = 0;
ss_end = 0;
t = 0;
complete_alpha = zeros(1,tmax);

De_change = zeros(1,tmax);
curve_trapz = zeros(1,tmax); % For estimating concavity

qsfine = zeros(ns, tmax);
grainpdf = zeros(ns, tmax);
y = zeros(ns, tmax);
%fan_elevation = nan(nel, tmax);
%dt_system_tects = nan(nel, tmax);
lambda = 0.3; % Porosity

Cv = 0.7;

M = 0*M;

topo_evo = zeros(tmax,ns+nn);
fan_topo_evo = {};
grain_evo = {};
differences = [];
for iel=1:(nel-1)
    M(iel,iel) = M(iel,iel)+Ce(1,1);
    M(iel,iel+1) = M(iel,iel+1)+Ce(1,2);
    M(iel+1,iel) = M(iel+1,iel)+Ce(2,1);
    M(iel+1,iel+1) = M(iel+1,iel+1)+Ce(2,2);
end

while iterate > 0
    t = t+1;
    
    % De - concetrative vs. diffusive processes
    if ss > 0
        % Steady state configuration
        complete_alpha(t) = ss_alpha_condition;
        De=c*((ss_alpha_condition)*lx)^nexp/kappa;
        dt = ss_dt;
    else            
        complete_alpha(t) = alpha(alpha_t);
        De=c*((alpha(alpha_t))*lx)^nexp/kappa;
        dt = alpha_dt;
    end
    
    De_change(t) = De;
    K = 0*K;
    F = zeros(nel,1);
    
    for iel=1:(nel-1)


        midx = coord_system(iel)+.5*h;
        D(iel) = 1 + De*(midx^nexp);
        k=zeros(nip,nip);
        
        for ni=1:nip
            k = k + dN'*D(iel)*dN./h;
        end
        
        K(iel,iel) = K(iel,iel)+k(1,1);
        K(iel,iel+1) = K(iel,iel+1)+k(1,2);
        K(iel+1,iel) = K(iel+1,iel)+k(2,1);
        K(iel+1,iel+1) = K(iel+1,iel+1)+k(2,2);

        % Find index for interpolated uplift value
        ek = iel*2;
        up(iel) = system_tects_interp(ek);
        f = [.5*up(iel)*h ;.5*up(iel)*h];
        
        F(iel) = F(iel)+f(1);
        F(iel+1) = F(iel+1)+f(2);

    end   
    
    if t == 1
        movement = system_tects*dt;
    else
        movement = displ + system_tects*dt;
    end
    
    displ_old = displ;
    
    A = M + dt*K;
    B = M*displ + dt*F;

    % SOLVE
    Free = 1:nel;
    Free([nel]) = [];
    displ = zeros(nel,1);
    displ([nel]) = rightbcval;
    
    A = sparse(A);
    
    U = chol(A(Free,Free));
    L = U';
    displ(Free) = U\(L\B(Free));
    tdispl(:,t) = displ;
        
    difference = movement - displ; 
    tdiff(:,t) = difference;
    
    dt_system_tects = system_tects*dt;
    
    if t == 1
        % One timestep where the fan elevation is simply the first movement
        fan_elevation = movement;
        topography = fan_elevation;
    else
        fan_elevation = [fan_elevation displ];
        
        tect_mat = repmat(dt_system_tects, 1, t); %produce a tectonic uplift
        % for every previous timestep

        disp_mat = repmat(displ, 1, t); %produce a displacement profile
        % for every previous timestep
        
        fan_elevation = fan_elevation + tect_mat;
        
        fs = size(fan_elevation);
        
        % Remove catchment topography greater that disp mat
        excess = find(fan_elevation > disp_mat);
        fan_elevation(excess) = disp_mat(excess);
        topography = fan_elevation(:,t);
        % Add fan sediment when lower than disp mat
        current_displacement = disp_mat(:,t);
        lower = find(topography < current_displacement);
        topography(lower) = current_displacement(lower);
    end
    
    fan_elev = fan_elevation(nn+1:end, :);
    %fan_topo_evo = [fan_topo_evo;fan_elev*lx];

    difference = movement - displ;
    erod = difference;
    depo = difference;
    
    erod(erod<=0) = 0;
    depo(depo>0) = 0;
   
    erod = erod;
    depo = -depo;
    terod(:,t) = erod(1:nn);
    tdepo(:,t) = depo(nn+1:end);
    
    erodq(t) = trapz(terod(:,t)*(h/dt)*kappa); % Get the area
    depoarea =  trapz(tdepo(:,t)*(h/dt)*kappa);
    depoq(t) = depoarea;
    qsfine(:, t) = depoarea - cumtrapz(tdepo(:,t)*(h/dt)*kappa);

    topo_current_dimensional = topography*lx;
    topo_evo(t,:) = topo_current_dimensional;
    
    curve = topo_current_dimensional(nn+1:end);
    straight = linspace(max(curve),min(curve),1000);
    curve_diff(t) = trapz(straight)-trapz(curve);

    fan_gradient = gradient(topo_current_dimensional(nn+1:end));
    catchment_gradient = gradient(topo_current_dimensional(1:nn));
    
    high_elev_catchment = max(topo_current_dimensional(1:nn));
    low_elev_catchment = min(topo_current_dimensional(1:nn));
    high_elev_fan = max(topo_current_dimensional(nn+1:end));
    low_elev_fan = min(topo_current_dimensional(nn+1:end));
    
    fan_gradient = (high_elev_fan-low_elev_fan)/(lx/2);
    catchment_gradient = (high_elev_catchment-low_elev_catchment)/(lx/2);
    
    mean_gradients_fan(t) = fan_gradient;
    mean_gradients_catchment(t) = catchment_gradient;
    slope = mean_gradients_catchment(t);
    
    A = (lx/1.4)^(1/0.6); % Hacks law

    % Long term discharge
    
    seconds_in_year = 31540000;
    if ss_end > 0
        % What proportion of days in a year are 'events'
        event_prop = seconds_in_year*alpha_events_prop(alpha_t);
        P = alpha(alpha_t)/event_prop;
    else
        P = ss_alpha_condition/31540000;
    end
    
    Q = P*lx; % We have already applied an infiltration threshold
    Qw(t) = Q;
    
    %disp(['Disp = ' num2str(Q)]);
    g = 9.8; % Gravity
    pf = 1000; % Water density
    ps = 1680; % Sediment density
    tc = 0.1; % Tcrit for gravel
    n = 0.03; % Manning's Roughness Coefficient for gravels
    
    
    tb = (Q^(3/5))*(n^(3/5))*pf*g*(slope^(7/10));
    tbs(t) = tb;
    
    D50 = (tb/(tc*(ps-pf)*g))*1000;
    
    D50s(t) = D50;

    D84 = D50*2;
    D84s(t) = D84;
    grainpdf0 = log(D50);
    phi0 = log(D84-D50);
        
    y(:,t) = (1-lambda)*cumtrapz(tdepo(:,t)./qsfine(:,t));
    y(qsfine(:,t)<0, t) = NaN;
    grainpdf(:,t) = grainpdf0+phi0*(1/Cv)*(exp(-C1*y(:,t))-1);
            
    if t > 2
        w = 1-(erodq(t)/depoq(t));
        
        if alpha_start
            alpha_t = alpha_t+1;
            if alpha_t > length(alpha)
                iterate = 0;
                if nowaitbars == 0
                    close(alphabar)
                end
            else
                if nowaitbars == 0
                    waitbar(alpha_t/length(alpha), alphabar);
                end
            end

        else
            if w < ss_threshold
                
                if nowaitbars == 0
                    close(ss_message);
                end
                
                ss_end = t;
                alpha_t = alpha_t+1;
                ss = 0;
                alpha_start = 1;
                t_finish = ss_end+length(complete_alpha);
                 
                if nowaitbars == 0
                    alphabar = waitbar(0,'Running alpha model');
                end
            end
        end
    end
end

% Final stratigraphy surface correction

disp_mat = repmat(displ, 1, t);

fan_elevation = [fan_elevation displ];

% Remove residual matrix values
fan_elevation = fan_elevation(:, 1:t+1);
fan_elevation = fan_elevation(nn+1:end, :);

fan_topo_evo = [fan_topo_evo;fan_elevation*lx]; %This can get very expensive
              
% Redimensionalise uplift for plotting
fan_elevation = fan_elevation*lx;


