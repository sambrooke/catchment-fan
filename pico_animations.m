load('./weather_station/InyoCounty.mat')
% Generate the time series for the glacial interglacial runs
inyo_names = unique(InyoCounty.NAME);
inyo_names = inyo_names(strcmp(inyo_names,'NAME') < 1);

% Based on indicent rainfall variance
load('./weather_station/variance_sorted_stations.mat');

% Limit stations to create smooth variance gradient
dists_sorted_limited = [dists_sorted(1:7);dists_sorted(25:30)];
variance_sorted_limited = [variance_sorted(1:7),variance_sorted(25:30)];
dists = dists_sorted_limited;

pds = cell(1, length(dists));
chances = zeros(1, length(dists));
pd_variances = zeros(1, length(dists));

for inyo_n = 1:length(dists)
    % Generate Distribution
    station_name = dists{inyo_n};
    rain = InyoCounty(InyoCounty.NAME==station_name,:);
    no_negs = rain.PRCP>-1;
    precip = rain.PRCP(no_negs)/1000; % Convert to metres/day
    no_precip = precip(precip<=0);
    precip = precip(precip>0);
    pd = fitdist(precip, 'lognormal');
    pds{inyo_n} = pd;
    chance = length(precip)/length(no_precip);
    chances(inyo_n) = chance;
    pd_variances(inyo_n) = variance_sorted_limited(inyo_n);
end


start_rain = 0.15;
rain_amplitude = 0.05;

cycles = 4;
t_step = 100;
t_len = 2000; % Cycles every 100kyr

% Cyclical
% Change from LGM to Holocene 
t_half = t_len/2;
t_cycle = (t_len/(t_half*t_len))*cycles;


shift_t = 1:1:t_len;
vq = rain_amplitude*sin(shift_t.*pi.*t_cycle)+start_rain;

% Sudden variance
variance_vq = ones(1,t_len)*start_rain;
variance_vq(400:800) = start_rain+rain_amplitude;
variance_vq(1400:1800) = start_rain+rain_amplitude;

max_inf = 1; % Holocene
min_inf = length(dists); % LGM

pd_choices_inverse = (1./variance_vq)./(max(variance_vq)).*max_inf;
pd_choices_inverse = ((pd_choices_inverse -max(pd_choices_inverse))./(max(pd_choices_inverse)-min(pd_choices_inverse)))*(max_inf-min_inf)+max_inf;
pd_choices_inverse(isnan(pd_choices_inverse)) = max(pd_choices_inverse);
pd_choices_inverse = fliplr(pd_choices_inverse);
pd_choices_inverse = ceil(pd_choices_inverse);

pd_choices = (variance_vq)./(max(variance_vq)).*max_inf;
pd_choices = ((pd_choices -max(pd_choices))./(max(pd_choices)-min(pd_choices)))*(max_inf-min_inf)+max_inf;
pd_choices(isnan(pd_choices)) = max(pd_choices);
pd_choices = fliplr(pd_choices);
pd_choices = ceil(pd_choices);

alpha = vq;

run_titles = {'mean_rainfall_no_variance','mean_rainfall_with_variance'};

base_struct = struct('incident', zeros(1,150),'t05', zeros(1,150),...
    't1', zeros(1,150),'t2', zeros(1,150));

glacial_sim_rainfall = struct(  );

for rt = 1:length(run_titles)
    glacial_sim_rainfall.(run_titles{rt}) = base_struct;
end

for nnn = 1:length(run_titles)
    
    run_name = run_titles{nnn};
    disp(run_name);
    if nnn == 1
        % Low variance
        % Variable mean
        
        pd_list = ones(1,length(alpha));
        run_alpha = alpha;
        pd_variances_list = pd_variances(pd_list);
        
    elseif nnn == 2
        
        % Variable variance - low in Interglacials
        % Variable mean
        pd_list = pd_choices;
        run_alpha = alpha;
        pd_variances_list = pd_variances(pd_list);
    end
    
    years = t_step; % Timestep
    

    alpha_dist = zeros(years,1);
    
   incident_mean = zeros(1,length(run_alpha)); % Incident rainfall mean
   eF_list_t1 = zeros(1,length(run_alpha)); % 1m threshold
   eF_list_t3 = zeros(1,length(run_alpha)); % 1m threshold
   eF_list_t5 = zeros(1,length(run_alpha)); % 1m threshold
   events_prop_list = zeros(1,length(run_alpha)); % Average proportion of effective events per year 
   variance_ky = zeros(length(run_alpha),1);

   for ky = 1:length(run_alpha)

       alpha_dist = zeros(years,1);

       thousand_year_rainfall = [];

       pd = pds{pd_list(ky)};
       chance = chances(pd_list(ky));

        for y=1:years
            precip = zeros(365,1);

            for d=1:length(precip)
                if rand <= chance
                    precip(d) = random(pd);
                end
            end

            thousand_year_rainfall = [thousand_year_rainfall; precip];
        end

        mean_target = run_alpha(ky);
        alpha_dist_norm = thousand_year_rainfall/max(thousand_year_rainfall);
        mean_orig = mean(alpha_dist_norm);
        alpha_mean_con = alpha_dist_norm*(mean_target/mean_orig);

        

        % New mean-conserving PDF
        no_precip = alpha_mean_con(alpha_mean_con<=0);
        precip = alpha_mean_con(alpha_mean_con>0);
        pd = fitdist(precip, 'lognormal');
        chance = length(precip)/length(no_precip);

        incident = zeros(years,1);
        events_prop = zeros(years,1);
        eF_t1 = zeros(years,1);
        eF_t2 = zeros(years,1);
        eF_t3 = zeros(years,1);
        
        for y=1:years % Every 1000 years

            precip_empty = 1;

            while precip_empty 
                precip = zeros(365,1);

                for d=1:length(precip)
                    if rand <= chance
                        precip(d) = random(pd);
                    end
                end

                precip(isnan(precip)) = 0;
                if trapz(precip) > 0
                    precip_empty = 0;
                end
            end
            
            % Second enforcement of mean rainfall target
            mean_target = run_alpha(ky);
            precip_norm = precip/max(precip);
            mean_orig = mean(precip_norm);
            precip_mean_con = precip_norm*(mean_target/mean_orig);
            
            variance_ky(ky) = std(precip_mean_con)^2;
            
            events_prop(y) = (sum(precip_mean_con>0)/365); % Proportion of days per year where stuff happens
            incident(y) = mean(precip_mean_con);
            eF_inf_1 = infiltration(precip_mean_con, 0.5, 0.5);
            eF_t1(y) = mean(eF_inf_1(~isnan(eF_inf_1)));
        end
        
        incident_mean(ky) = mean(incident); % Incident rainfall mean
        eF_list_t1(ky) = mean(eF_t1); % 0.5m threshold
        events_prop_list(ky) = mean(events_prop);
    end
    
    glacial_sim_rainfall.(run_name).variance = variance_ky;
    glacial_sim_rainfall.(run_name).incident = incident_mean;
    glacial_sim_rainfall.(run_name).t05 = eF_list_t1;
    glacial_sim_rainfall.(run_name).events_prop = events_prop_list;
end

save('./data/pico_glacial_interglacial.mat', 'glacial_sim_rainfall');

