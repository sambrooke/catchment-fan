filename = '../data/inyo_stations.csv';
delimiter = ',';
startRow = 2;
formatSpec = '%q%f%f%f%f%f%f%f%f%f%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string', 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
fclose(fileID);
inyostations = table(dataArray{1:end-1}, 'VariableNames', {'stations','latitude','longitude','elevation','mean_rainfall','variance','mu','sigma','stdevs','chance_list'});
clearvars filename delimiter startRow formatSpec fileID dataArray ans;

% 
% arid_idx = strcmp(inyostations.stations,'DEATH VALLEY, CA US');
% arid_mu = inyostations.mu(arid_idx);
% arid_sig = inyostations.sigma(arid_idx);
% 
% 
% wet_idx = strcmp(inyostations.stations,'BISHOP AIRPORT, CA US');
% wet_mu = inyostations.mu(wet_idx);
% wet_sig = inyostations.sigma(wet_idx);
% 
% figure;
% x = 0:0.01:50;
% y = lognpdf(x,wet_mu,wet_sig);
% plot(x,y);
% 
% hold on;
% y = lognpdf(x,arid_mu,arid_sig);
% plot(x,y);
% 
% legend('Wet', 'Arid');
% grid on
% xlabel('x')
% ylabel('y')

figure;

mean_mu = mean(inyostations.mu);
mean_sig = mean(inyostations.sigma); 
x = 0:0.01:50;
y = lognpdf(x,mean_mu,mean_sig);
plot(x,y);
