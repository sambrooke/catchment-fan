addpath('../processing');
addpath('../plotting');
addpath('../lib');
load('../weather_station/InyoCounty.mat');

fig2 = figure();

X = 20;
Y = 30;
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# fig6ure size on paper (width & height)
ySize = Y - 2*yMargin;     %# fig6ure size on paper (width & height)

set(fig2,'visible','off');
set(fig2, 'PaperSize',[X Y]);
set(fig2, 'PaperPosition',[0 yMargin xSize ySize])
set(fig2, 'PaperUnits','centimeters');

[haa, pos] = tight_subplot(5, 1, [0.02,0.02], [0.3 0.05], [0.1 0.05]);
col = [1 2 3 4 5];
colours = ['b', 'm', 'r'];

% Set subplot size
buffer_n = 0;

for jj = 1:5
    haa(jj).ActivePositionProperty = 'outerposition';
    if jj <= 2
        current_pos = haa(col(jj)).Position;
        new_pos = [current_pos(1), current_pos(2),...
            current_pos(3), current_pos(4)];
        set(haa(col(jj)), 'Position', new_pos);
    end

    if jj > 3
        current_pos = haa(jj).Position;
        height_change = 0.08;
        position_change = height_change*buffer_n;
        buffer_n = buffer_n+height_change;
        new_pos = [current_pos(1), current_pos(2)-buffer_n,...
            current_pos(3), current_pos(4)+height_change];
        set(haa(jj), 'Position', new_pos);
    end
end


load('../data/threshold_runs/threshold_incident');

axes(haa(1));
plot(1:1:plot_len,run_alphas(1,:), colours(1));
hold on;


for rn = 2:3
   plot(1:1:plot_len,run_alphas(rn,:), colours(rn));
   hold on;
end

% Plot Mean rainfall runs
ylim([0.02 0.12]);
grid on;
yticks([0.02, 0.04 0.06 0.08 0.1]);
xticklabels([]);
ylabel('\alpha');
xlabel('ka');
legend('Incident', 'C_{max} = 0.5 m', 'C_{max} = 1 m');

axes(haa(2));

for rn = 1:3
   load(['../data/threshold_runs/' run_names{rn}]);
   plot(1:1:plot_len,erodq(ss_end:(ss_end-1+plot_len)), colours(rn));
   hold on;
end

ylim([0.5 1.5]);
ylabel('qs m^2y^{-1}');
xlabel('ka');

legend('Incident', 'C_{max} = 0.5 m', 'C_{max} = 1 m');

axes(haa(3));

load('../data/inyo_time_series.mat');
variance = [repmat(inyo_times_series_variance(1,:),1,10) ...
    repmat(inyo_times_series_variance(20,:),1,10) ...
    repmat(inyo_times_series_variance(1,:),1,10)];

plot(1:1:plot_len,variance, 'k');

ylim([0 3]);
grid on;
%yticks([0 0.4 0.6 0.8 1 ]);
xticklabels([]);
ylabel('\sigma^2');

strat_axes = [4, 0, 5];
for rn = [1,3]
   axes(haa(strat_axes(rn)));
   load(['../data/threshold_runs/' run_names{rn}]);
   grainpdf_mm = exp(grainpdf');
   max_size = 100;
   
   if rn > 1
    t_step = 100;
    dx = 100;       
   else
    t_step = 100;
    dx = 100;       
   end
   
   plot_params = struct(...
    'ylim_params' , [-2500 500],...
    'y_interval', 500,...
    't_step' , t_step,...
    'dx' , dx);
   patch_plot_nofig;
   ylabel('Elevation (m)');
   xlabel('Distance (m)');
   title([]);
end

for j = 1:length(ha)
    set(haa(j), 'FontSize', 12);
end

fig2.Renderer = 'Painters';
plot_destination = './pdfs';
print(fig2, [plot_destination filesep 'fig_threshold.pdf'],'-dpdf');
