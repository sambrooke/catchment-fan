clc;
clear;

addpath('../lib');
addpath('../data');
addpath('./sensitivity_analyses');

topo_sensitivity_plots;

n1_sensitivity_plots;

cn_alpha_sensitivity_plots;