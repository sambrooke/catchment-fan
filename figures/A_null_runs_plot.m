colormap jet;
X = 29;
Y = 21;               
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# figure size on paper (width & height)
ySize = Y - 2*yMargin;     %# figure size on paper (width & height)
% Start at which timestep
addpath('../lib');
load('../data/null_runs/null_run_rainfall.mat')
plts = {'mean_pulse', 'variance_pulse'};


for pl = 1:2
    
    sub_runs = fieldnames(null_run_rainfall.(plts{pl}));
    suite_data = null_run_rainfall.(plts{pl});
    plt_name = plts{pl};
    
    fig = figure();
    set(fig,'visible','off');
    set(fig, 'PaperSize',[X Y]);
    set(fig, 'PaperPosition',[0 yMargin xSize ySize])
    set(fig, 'PaperUnits','centimeters');

    columns = [[1,4,7,10,13,16];...
        [2,5,8,11,14,17];[3,6,9,12,15,18]];
    alphabet = {{'a','b','c','d','e','f'},{'g','h','i','j','k','l'},...
        {'m','n','o','p','q','r'}};
    [ha, pos] = tight_subplot(6, 3, [0.02,0.01], [0.3 0.05], [0.1 0.05]);

    for sub_num = 1:length(sub_runs)
        
        sub_data = suite_data.(sub_runs{sub_num});
        
        col = columns(sub_num,:);
        alphabet_list = alphabet{sub_num};
        main_title_out = sub_runs{sub_num};
        main_title = main_title_out;

        buffer_n = 0;
        for jj = 1:6
            ha(col(jj)).ActivePositionProperty = 'outerposition';
            if jj == 2
                current_pos = ha(col(jj)).Position;
                new_pos = [current_pos(1), current_pos(2)+0.04,...
                    current_pos(3), current_pos(4)-0.04];
                set(ha(col(jj)), 'Position', new_pos);
            end

            if jj == 3
                current_pos = ha(col(jj)).Position;
                new_pos = [current_pos(1), current_pos(2)+0.035,...
                    current_pos(3), current_pos(4)+0.005];
                set(ha(col(jj)), 'Position', new_pos);
            end

            if jj > 3
                current_pos = ha(col(jj)).Position;
                height_change = 0.07;
                position_change = height_change*buffer_n;
                buffer_n = buffer_n+height_change;
                new_pos = [current_pos(1), current_pos(2)-buffer_n,...
                    current_pos(3), current_pos(4)+height_change];
                set(ha(col(jj)), 'Position', new_pos);
            end
        end
        
        
        runs_titles = {'Capacity = 0', 'Capacity = 1 m', 'Capacity = 3 m'};
        run_names = {'incident', 't1', 't3'};
        run5_colours = cool(4);

        plot_len = 150;
    %     axes(ha(col(1)));
    %     plot(1:1:plot_len,alpha_incident,'Color',run5_colours(1,:));
    %     hold on;
        alpha_variance = sub_data.variance;
        alpha_incident = sub_data.incident.alpha;

        for rn = 1:length(run_names)
            
            run_data = sub_data.(run_names{rn});
            alpha = run_data.alpha;
            ss_end = run_data.ss_end;
            erodq = run_data.erodq;
            
            axes(ha(col(1)));
            plot(1:1:plot_len,alpha, 'Color', run5_colours(sub_num+1,:));
            hold on;

            axes(ha(col(3)));
            plot(1:1:plot_len,erodq(ss_end+1:plot_len+ss_end), 'Color', run5_colours(sub_num,:));
            hold on;

        end

        axes(ha(col(3)));
        xticks(0:20:plot_len);
        xticklabels(plot_len:-20:0);
        xlabel('Time (ka)');
        if pl == 1
            ylabel('qs m^2ka^{-1}');
        else
            set(ha(col(3)),'YTickLabel',[]);
        end
        textLoc(['\bf ' alphabet_list{3}],  {'northwest',1/100,1/10});
        %legend(runs5_names);
        ylim([0 5]);
        grid on;

        axes(ha(col(2)));
        plot(1:1:plot_len,alpha_variance, 'Color', [.3 .3 .3]); 
        xticks(0:20:plot_len);
        xticklabels(plot_len:-20:0);
        %xlabel('Time (ka)');
        set(ha(col(2)),'XTickLabel',[]);
        yticks([0 1]);
        if pl == 1
            ylabel('\sigma^2');
        else
            set(ha(col(2)),'YTickLabel',[]);
        end
        textLoc(['\bf ' alphabet_list{2}], {'northwest',1/100,1/10});
        ylim([-0.1 .25]);
        grid on;

        axes(ha(col(1)));
        xticks(0:20:plot_len);
        xticklabels(plot_len:-20:0);
        %xlabel('Time (ka)');
        yticks(0:0.1:3);
        if pl == 1
            ylabel('\alpha (m)');
            yticklabels(0:0.1:3);
        else
            set(ha(col(1)),'YTickLabel',[]);
        end
        
        set(ha(col(1)),'XTickLabel',[]);

        textLoc(['\bf ' alphabet_list{1}], {'northwest',1/100,1/10});
        %legend('Incident Rainfall', 'Effective Rainfall');
        ylim([0 4]);
        %legend(['Incident Rainfall', runs5_names]);
        grid on;
        sb_coords = [4,5,6];

        for rn = 1:length(run_names)
            run_data = sub_data.(run_names{rn});

            run_name5 = run_names{rn};
            
            grainpdf =  run_data.grainpdf;
            fan_elevation =  run_data.fan_elevation;
            coord_basin =  run_data.coord_basin;
            alpha_realstep =  run_data.alpha_realstep;
            alpha =  run_data.alpha;
            erodq =  run_data.erodq;
            lx =  run_data.lx;
            ss_end = run_data.ss_end;
            kappa = run_data.kappa;
            nexp = run_data.nexp;
            c = run_data.c;
            ss_alpha_condition = run_data.ss_alpha_condition;
            ss_realstep = run_data.ss_realstep;
            alpha_dt = run_data.alpha_dt;
            ss_dt =  run_data.ss_dt;
            full_duration = run_data.full_duration;
            u0 =  run_data.u0;
            
            gcolour = exp(grainpdf')./max(exp(grainpdf(:)));
            
            run_title = run_name5;
            
            grainpdf_mm = exp(grainpdf');
            max_size = max(max(grainpdf_mm(ss_end+10:plot_len+ss_end, :)));
            max_size = ceil(max_size);
            max_size = 60*10;
            cm = jet(max_size);
            colormap(cm);

            plot_t_start = ss_end+1;



            % Y step size

            t_step = 50;
            t_start = ss_end+1;

            % X step size
            dx = 50;

            axes(ha(col(sb_coords(rn))));
            ha(col(sb_coords(rn))).ActivePositionProperty = 'outerposition';
            % Plot stratigraphy

            fan_heights = fan_elevation;
            
            sg = size(grainpdf_mm);
            fh = size(fan_heights);
            % t_end = fs_y;
            % x_end = fs_x;
            t_end = fh(2);
            x_end = sg(2);
            
            fan_x = coord_basin*lx;
            fan_dx = fan_x(x_end)/max(x_end);
            ss_aquisition = plot_t_start;

            alpha_x = 1:1:length(alpha);
            alpha_x = alpha_x.*alpha_realstep./1e3;
            alpha_x = alpha_x((plot_t_start):end);

            maxPrecip = max(alpha);
            maxErosion = max(erodq(t_start:plot_len+ss_end));

            for y=t_start:t_step:t_end

                y_b = y;
                y_t = y+t_step;
                if y_t > t_end
                    y_t = t_end;
                end

                for x=1:dx:x_end
                    x_l = x;
                    x_r = x+dx;

                    if x_r > x_end
                        x_r = x_end;
                    end

                    % SWAPPED fan_elevation(t,:) to fan_elevation(:,t)
                    xco = [fan_x(x_l) fan_x(x_r) fan_x(x_r) fan_x(x_l)];
                    zco = [fan_heights(x, y_t), fan_heights(x_r,y_t), fan_heights(x_r,y_b), fan_heights(x,y_b)];

                    colourID = mean(mean(grainpdf_mm(y_b:y_t,x_l:x_r),'omitnan'),'omitnan');
                    %colourID = max(max(grainpdf_mm(y_b:y_t,x_l:x_r)));

                    if isnan(colourID)
                        colour = [1 1 1];
                    else
                        color_ceil = ceil(colourID*10);
                        if color_ceil > max_size
                            colour = cm(end,:);
                        else
                            colour = cm(color_ceil,:);
                        end
                    end

                    if sum(colour) == 0
                       disp('No colour!');
                    end
                    h = patch(xco, zco, colour, 'EdgeColor', 'none');
                    drawnow
                end

            end

    %         marner_gs = [30, 20, 10];
    % 
    %         for mgs = 1:length(marner_gs)
    %             x_coords = [];
    %             y_coords = [];
    %             for y=ss_end+1:1:t_end
    %                 [cvv index] = min(abs(grainpdf_mm(y,:)-marner_gs(mgs)))
    % 
    %                 x_coords = [x_coords; fan_x(index)];
    %                 y_coords = [y_coords; fan_heights(index, y)];
    %             end
    % 
    %             line(x_coords, y_coords,'LineWidth',0.5, 'Color', [0 0 0]);
    %         end

            %Time Slices (thousands of years)

    %         ka_slices = [20 40 60 80];
    %         for kslice = 1:length(ka_slices)
    %             ka_line = fan_heights(:, ka_slices(kslice));
    %             line(1:fan_dx:fan_x(x_end), ka_line,'LineWidth',0.5, 'Color', [1 1 1]);    
    %         end



            ss_realstep = lx^2/kappa*ss_dt;
            alpha_realstep = lx^2/kappa*alpha_dt;
            real_alpha_max = alpha_realstep * full_duration;
            real_ss_max = ss_realstep*plot_t_start;
            yticks(-100:100:300);
            textLoc(['\bf ' alphabet_list{sb_coords(rn)}], {'northwest',1/100,1/10});

%             if prefix == 1
%                 ylabel('Metres');
%                %yticklabels(-100:100:300);
%             end

            if rn == 1
                hcb=colorbar('north');
                set(hcb,'YTick', 0:.2:1);
                set(hcb,'YTickLabel',linspace(0,ceil(max_size/10),6));
                ylabel(hcb,'Grain Size (mm)');
            end

            xticks(0:500:2000);

            if rn == 3
                xlabel('Km');
                xticklabels(0:0.5:2);
            end

            custom_lines = [t_start, t_end];

            for ts = 1:length(custom_lines)
                cs_line = fan_heights(:, custom_lines(ts));
                line(1:fan_dx:fan_x(x_end), cs_line,'LineWidth',1.3, 'Color', [0 0 0]);
            end
            grid on;

    %         run_params = {['\bf kappa', '\rm ',  num2str(kappa), '\bf   ss time  ', '\rm ',  [num2str(real_ss_max/1e6) 'Ma']], ...
    %             ['\bf n ', '\rm ',  num2str(nexp), '\bf   run time  ', '\rm ',  [num2str(real_alpha_max/1e3) 'kyr']], ...
    %             ['\bf c  ', '\rm ',  num2str(c)], ...
    %             ['\bf catchment uplift  ', '\rm ',  num2str(u0), 'my^{-1}'], ...
    %             ['\bf catchment length  ', '\rm ',  num2str(lx) 'm']
    %         };

    %         h1 = textLoc(run_params,{'northeast', 1/10}, 'EdgeColor', 'black', 'FontSize', 8);
            %ylim([-100 300]);   
        end

        plot_destination = './pdfs';


    end

    a= annotation('textbox', [0 0.89 1 0.1], ...
        'String', plt_name, ...
        'EdgeColor', 'none', ...
        'HorizontalAlignment', 'center');
    set(a,'interpreter','none')

    fig.Renderer = 'Painters';
    print(fig, [plot_destination filesep 'variance_pulses_ensemble' '.pdf'],'-dpdf');
end
