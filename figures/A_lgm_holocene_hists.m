X = 10;
Y = 10; 
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# figure size on paper (width & height)
ySize = Y - 2*yMargin;     %# figure size on paper (width & height)
% Start at which timestep

fig = figure();

set(fig,'visible','off');
set(fig, 'PaperSize',[X Y]);
set(fig, 'PaperPosition',[0 yMargin xSize ySize])
set(fig, 'PaperUnits','centimeters');

lgm_average = geotiffread('../data/inyo_climate/lgm_inyo_average.tif');
lgm_average(lgm_average<0) = NaN;
lgm_average = lgm_average(lgm_average>0);
lgm_average = lgm_average(isnan(lgm_average) < 1);

lgm_average = double(lgm_average(:));

XGrid = 0:1:70;
pd1 = fitdist(lgm_average, 'lognormal');
YPlot_lgm = pdf(pd1,XGrid);
hLine1 = plot(XGrid,YPlot_lgm,'Color','b',...
    'LineStyle','-', 'LineWidth',2,...
    'Marker','none', 'MarkerSize',6);

hold on;

current_average = geotiffread('../data/inyo_climate/current_precipi_inyo_resample.tif');
current_average(current_average<0) = NaN;
current_average = current_average(current_average>0);
current_average = current_average(isnan(current_average) < 1);
current_average = double(current_average(:));

pd1 = fitdist(current_average, 'lognormal');
YPlot_current = pdf(pd1,XGrid);
hLine2 = plot(XGrid,YPlot_current,'Color','r',...
    'LineStyle','-', 'LineWidth',2,...
    'Marker','none', 'MarkerSize',6);

hold on;


mean_lgm = floor(mean(lgm_average));
mean_lgm_y = YPlot_lgm(mean_lgm+1);
mean_holo = floor(mean(current_average));
mean_holo_y = YPlot_current(mean_holo+1);

plot([mean_lgm mean_lgm], [0 mean_lgm_y], 'b--');
hold on
plot([mean_holo mean_holo], [0 mean_holo_y], 'r--');

string1 = ['Mean = ' num2str(mean_lgm)];
string2 = ['Mean = ' num2str(mean_holo)];
text(mean_lgm+1,mean_lgm_y,string1, 'Color', 'b');
text(mean_holo+1,mean_holo_y,string2, 'Color', 'r');

ylabel('f');
xlabel('Mean Precipitation (mm)');
legend([hLine1, hLine2], {'Last Glacial Maximum', 'Present'});

xlim([0 60]);

plot_destination = './pdfs/';
print(fig, [plot_destination filesep 'lgm_holocene_hists.pdf'],'-dpdf');

