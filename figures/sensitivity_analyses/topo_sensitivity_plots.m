X = 25;
Y = 8;                  
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# figure size on paper (width & height)
ySize = Y - 2*yMargin;     %# figure size on paper (width & height)
% Start at which timestep

fig = figure();

%set(fig,'visible','off');
set(fig, 'PaperSize',[X Y]);
set(fig, 'PaperPosition',[0 yMargin xSize ySize])
set(fig, 'PaperUnits','centimeters');
set(fig, 'Visible','off');

runs = subdir(['../data/sensitivity_runs' filesep 'alpha_runs' filesep '*.mat']);

alpha_slopes = zeros(length(runs),1);
alpha_means = zeros(length(runs),1);

legend_items = [];
legend_labels = {};
cm = flipud(cool(length(runs)+10));

subplot(1,2,1);
gradients = zeros(1,length(runs));
alphas = zeros(1,length(runs));

for rr = 1:length(runs)
    
    load(runs(rr).name);
    
    tplot = plot(topo_current_dimensional(:,end), ...
        'Color', cm(rr,:));
    
    %gradients(rr) = rad2deg(atan(mean_gradients_catchment(ss_end+1)));
    gradients(rr) = mean_gradients_catchment(ss_end+1);

    alphas(rr) = ss_alpha_condition;
    
    legend_items = [legend_items; tplot];
    legend_labels = [legend_labels; ...
        [num2str(ss_alpha_condition) ' m/yr']];
    hold on;
end

ylim([0,250]);

legend(legend_items,legend_labels);

alphas = alphas(2:end);
gradients = gradients(2:end);
xlabel('Distance (m)');
ylabel('Elevation (m)');

hold on;
plot([1000,1000], [0 500], 'k--');

subplot(1,2,2);
[xData, yData] = prepareCurveData( alphas, gradients );

% Set up fittype and options.
ft = fittype( 'power1' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [0.00886573828674494 -0.99296786547882];

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );
% Plot fit with data.
h = plot( fitresult, xData, yData );
legend( h, 'gradients vs. alphas', 'Fit', 'Location', 'NorthEast' );
% Label axes
xlabel('Precipitation m/yr');
ylabel('Steady State Slope');
grid on
set(gca, 'fontsize',11);

print(fig, ['./pdfs' filesep 'topo_sensitivity' '.pdf'],'-dpdf')
