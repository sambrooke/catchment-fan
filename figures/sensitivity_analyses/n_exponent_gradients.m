% inyo_times_series_variance - ka mean annual variance
% daily_station_variance - input variance per ka
% inyo_time_series_incident - 50ka incident rainfall for 0.1m/yr
% inyo_time_series_t1 - 50ka effective for 1m depth
% inyo_time_series_t3 - 50ka effective for 3m depth
% inyo_time_series_t5 - 50ka effective for 15m depth
% inyo_times_series_stations - Weather tation names

alpha_A = inyo_time_series_t3(1,:);
alpha_B = inyo_time_series_t3(31,:);

fig = figure;
X = 29/2;
Y = 21/2;
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# fig6ure size on paper (width & height)
ySize = Y - 2*yMargin;     %# fig6ure size on paper (width & height)    

set(fig, 'PaperSize',[X Y]);
set(fig, 'PaperPosition',[0 yMargin xSize ySize])
set(fig, 'PaperUnits','centimeters');
    
c_list = [1e-2, 1e-4,1e-6];
n_list = [1,1.5,2];

[xData, yData] = prepareCurveData( c_list, n_list );

% Set up fittype and options.
ft = fittype( 'power2' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [0.765381821787007 -0.0688634336100726 0.00799385597193406];

% Fit model to data.
[fd, gof] = fit( xData, yData, ft, opts );

c_choice = [1e-6:1e-6:0.01];
n_choice = fd.a.*c_choice.^fd.b+fd.c;
n_points = 1:0.1:1.2;

legend_labels = {};
legend_items = [];


% Field slopes

fannames = fieldnames(distance_sorted);

% f = figure('Menubar','none');
% set(f, 'PaperSize',[X Y]);
% set(f, 'PaperPosition',[0 yMargin xSize ySize])
% set(f, 'PaperUnits','centimeters');


start_points = struct('G10', [36.7765850388, -117.1271739900], 'G8',[36.77300833333333, -117.11081944444445], ...
    'T1', [36.57948, -117.103], 'SR1', [33.317537, -116.177939]);

slope_data = struct();

gradients = [];

lx = 3000;

for fn=1:length(fannames)

    if strcmp(fannames{fn}, 'T1') > 0
       dem = grotto_dem;
    elseif strcmp(fannames{fn}, 'SR1') > 0
       dem = santa_rosa_dem; 
    else
       dem = grape_dem;
    end

    slope_data.(fannames{fn}) = struct();

    cf = distance_sorted.(fannames{fn});
    start = start_points.(fannames{fn});

    [start_x,start_y,u1,utm1] = wgs2utm(start(1),start(2)); 

    s_names = fieldnames(cf);
    legend_labels = {};
    legend_items = [];

    for sn=1:length(s_names)
       dat = cf.(s_names{sn});
       coords = cell2mat(dat(:,3));

       [dn,z,xx,yy] = demprofile(dem, 50, coords(:,1), coords(:,2));

       distances = arrayfun(@(x,y) sqrt((x-start_x)^2+(y-start_y)^2), xx, yy);

       [dist_sort,I] = sort(distances);
       z_sort = z(I);

       mdl = fitlm(dist_sort,z_sort,'linear');
       c = mdl.Coefficients.Estimate(1);
       x1 = mdl.Coefficients.Estimate(2);

       dist_all = 0:1:5000;
       zy = x1.*dist_all+c;
       
       plot((dist_sort./max(dist_sort))*lx,z_sort-z_sort(end), '-', ...
           'Color', [.5,.5,.5], 'LineWidth', 1);
       hold on;
    end
end

cmap = winter(length(n_points));
for ndx = 1:length(n_points)
   
    tmax = 200;
    
    [kappa,c,nexp,lx,u0,alpha_realstep,C1,ss_yield] = reset_params;
    min_diff = abs(n_choice-n_points(ndx));
    nx = find(abs(min_diff)==min(abs(min_diff)));
    
    lx = 3000;
    
    c = c_choice(nx);    
    nexp = n_points(ndx);
    
    kappa = 0.005;
    u0 = 0.002;
    C1 = 0.55;
    alpha = [ones(1,100)*0.1, ones(1,100)*0.5];
    ss_alpha_condition = 0.1;

    fan_evo;
    p1 = plot(linspace(0,lx,1000),topo_evo(150,nn+1:end), ...
        'Color', cmap(ndx,:), 'LineWidth', 2);
    hold on;
    legend_items = [legend_items; p1];
    legend_labels = [legend_labels; num2str(nexp)];
end

legend(legend_items,legend_labels);
xlim([1 3000]);
ylim([0 500]);

run_title = 'n_exponent_gradients';
plot_destination = './figures/pdfs';
print(fig, [plot_destination filesep run_title '.pdf'],'-dpdf')