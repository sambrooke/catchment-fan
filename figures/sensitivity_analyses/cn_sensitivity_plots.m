clear;
clc;

X = 10;
Y = 10;                  
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# figure size on paper (width & height)
ySize = Y - 2*yMargin;     %# figure size on paper (width & height)
% Start at which timestep

fig = figure();


%set(fig,'visible','off');
set(fig, 'PaperSize',[X Y]);
set(fig, 'PaperPosition',[0 yMargin xSize ySize])
set(fig, 'PaperUnits','centimeters');
runs_1 = subdir(['sensitivity_runs' filesep 'cn_runs' filesep '*.mat']);

subplot(3,1,2:3);

l_items = [];
l_labels = {};
cm = winter(length(runs_1));

for rr = 1:length(runs_1)
    load(runs_1(rr).name);
    
    if nexp == 1
        ss_time = ((1:1:ss_end).*ss_realstep)./1e6;
        time = (((1:1:length(erodq)-ss_end).*alpha_realstep)./1e6)+ss_time(end);
        time = [ss_time, time];
        time_buffer = ((1:1:(189-41)).*ss_realstep)./1e6;
        time = [time_buffer,time+time_buffer(end)];
        erodq = [zeros(1,(189-41)),erodq];
        erodq(189-41) = 1.0000e-10;
        disp(max(erodq));
        gplot = plot(time,erodq, 'Color', 'k', 'LineWidth', 1.2);
    else
        ss_time = ((1:1:ss_end).*ss_realstep)./1e6;
        time = (((1:1:length(erodq)-ss_end).*alpha_realstep)./1e6)+ss_time(end);
        time = [ss_time, time];
        erodq(1) = 1.0000e-10;
        disp(max(erodq));
        gplot = plot(time,erodq, 'Color', 'b', 'LineWidth', 0.3);
    end
    
    l_items = [l_items; gplot];
    l_labels = [l_labels; ...
        ['n = ' num2str(nexp)]];
    hold on;
end

xlabel('Time (Ma)');
ylabel('Qs (m^2/yr)');
plot([0 50], [erodq(ss_end+1),erodq(ss_end+1)], '--');
xlim([140 280]);
ylim([1 1000]);
grid on
set(gca, 'Yscale', 'log');

legend(l_items, l_labels);

subplot(3,1,1);
ss_alpha = ones(1,(189-41)).*ss_alpha_condition;
alpha_all = [ss_alpha';alpha];
time_alpha = (((1:1:length(erodq)-ss_end).*alpha_realstep)./1e6)+ss_time(end);
time_ss =  ((1:1:(189-41)).*ss_realstep)./1e6;
plot([time_ss,time_alpha],alpha_all);
xlim([140 280]);
xlabel('Time (Ma)');
yl = ylabel('$\bar{\alpha}$ (m/yr)');
set(yl,'interpreter','Latex')
grid on
%print(fig, ['./plots' filesep 'cn_sensitivity' '.pdf'],'-dpdf');
