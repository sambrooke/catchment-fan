clear;
clc;
run1 = subdir(['../data/sensitivity_runs' filesep ...
    'cn_alpha_runs' filesep 'n1' filesep '*.mat']);
run2 = subdir(['../data/sensitivity_runs' filesep ...
    'cn_alpha_runs' filesep 'n2' filesep '*.mat']);

figure;
legend_items = [];
legend_labels = {};
cm = winter(length(run1));

run1_erodq = [];
run2_erodq = [];
run1_alphas = [];
run2_alphas = [];

for rr = 1:length(run1)
    load(run1(rr).name);
    run1_erodq = [run1_erodq; erodq(ss_end+2)];
    run1_alphas = [run1_alphas; ss_alpha_condition];
end

for rr = 1:length(run2)
    load(run2(rr).name);
    run2_erodq = [run2_erodq; erodq(ss_end+2)];
    run2_alphas = [run2_alphas; ss_alpha_condition];
end

p1 = plot(run1_alphas, run1_erodq, 'x');
hold on;
p2 = plot(run2_alphas, run2_erodq, 'x');
ylim([0 10]);

legend([p1, p2],{'n1', 'n2'});
