clear;
clc;

X = 10;
Y = 10;                  
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# figure size on paper (width & height)
ySize = Y - 2*yMargin;     %# figure size on paper (width & height)
% Start at which timestep

fig = figure();

%set(fig,'visible','off');
set(fig, 'PaperSize',[X Y]);
set(fig, 'PaperPosition',[0 yMargin xSize ySize])
set(fig, 'PaperUnits','centimeters');


runs = subdir(['sensitivity_runs' filesep 'C1_runs' filesep '*.mat']);

legend_items = [];
legend_labels = {};
cm = winter(length(runs)+3);

for rr = 1:length(runs)
    load(runs(rr).name);
    gplot = plot(coord_basin_dx, exp(grainpdf(:,ss_end+1)), 'Color', cm(rr,:));
    legend_items = [legend_items; gplot];
    legend_labels = [legend_labels; ...
        [num2str(C1)]];
    hold on;
end

legend(legend_items,legend_labels);
set(gca, 'fontsize',11);
xlabel('Distance (m)');
ylabel('Grain size (mm)');
grid on
print(fig, ['./plots' filesep 'c1_sensitivity' '.pdf'],'-dpdf')
