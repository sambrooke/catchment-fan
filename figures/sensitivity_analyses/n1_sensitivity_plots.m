X = 25;
Y = 10;               
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# figure size on paper (width & height)
ySize = Y - 2*yMargin;     %# figure size on paper (width & height)
% Start at which timestep

fig = figure();

%set(fig,'visible','off');
set(fig, 'PaperSize',[X Y]);
set(fig, 'PaperPosition',[0 yMargin xSize ySize])
set(fig, 'PaperUnits','centimeters');
set(fig, 'Visible','off');

alpha_means = [0.01, 0.1, 0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
runs = subdir(['../data/sensitivity_runs' filesep 'n1_alpha_runs' filesep '*.mat']);

legend_items = [];
legend_labels = {};
legend_items2 = [];
legend_labels2 = {};
cm = flipud(winter(length(runs)+3));
file_names2 = cell(length(runs),1);

for rr = 1:length(runs)
    suffix = regexp(runs(rr).name, '([0-9][.][0-9]+.mat$)|([0-9]+.mat)$', 'match');
    suffix = suffix{1};
    file_names2{find(alpha_means==str2num(suffix(1:end-4)))} = runs(rr).name;
end

for rr = 1:length(file_names2)
    load(file_names2{rr});
    time_alpha = (((1:1:length(erodq)).*alpha_realstep))./1e3;
    
    subplot(1,2,2);
    gplot = plot(time_alpha, erodq, 'Color', cm(rr,:));
    legend_items = [legend_items; gplot];
    legend_labels = [legend_labels; ...
        [num2str(cycle_amp)]];
    hold on;
    subplot(1,2,1);
    ss_alpha = ones(ss_end,1)*ss_alpha_condition;
    alph_plot = plot(time_alpha,[ss_alpha;alpha], 'Color', cm(rr,:));
    hold on;
    legend_items2 = [legend_items2; alph_plot];
    legend_labels2 = [legend_labels2; ...
        [num2str(cycle_amp)]];
end

subplot(1,2,2);
xlim([ss_end+1 ss_end+300]);
ylim([0 50])
legend(legend_items,legend_labels);
set(gca, 'fontsize',11);
xlabel('Time (ka)');
ylabel('qs (m^2/yr)');
grid on
subplot(1,2,1);
xlim([ss_end+1 ss_end+300]);
legend(legend_items2,legend_labels2);
set(gca, 'fontsize',11);
xlabel('Time (ka)');
yl = ylabel('$\bar{\alpha}$ (m/yr)');
set(yl,'interpreter','Latex')
grid on
print(fig, ['./pdfs' filesep 'n1_sensitivity' '.pdf'],'-dpdf')
