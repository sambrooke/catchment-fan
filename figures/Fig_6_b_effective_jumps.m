addpath('../lib');
addpath('../data');
X = 15;
Y = 10;                  
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# fig3ure size on paper (width & height)
ySize = Y - 2*yMargin;     %# fig3ure size on paper (width & height)
% Start at which timestep

fig = figure();

set(fig, 'PaperSize',[X Y]);
set(fig, 'PaperPosition',[0 yMargin xSize ySize])
set(fig, 'PaperUnits','centimeters');

% effective_runs1 = subdir('../data/effective_jumps/k0.001/*.mat');
% %erod_diffs(erod_diffs>0) = log10(erod_diffs(erod_diffs>0));
% %erod_diffs(erod_diffs<0) = -log10(erod_diffs(erod_diffs<0));    
% erod_diffs = zeros(length(effective_runs1),1);
% eff_ratios = zeros(length(effective_runs1),1);
% 
% for efn = 1:length(effective_runs1)
%     load(effective_runs1(efn).name);
%     erod_diff = (erodq(ss_end+51))-erodq(ss_end);
%     erod_diffs = [erod_diffs; erod_diff];
%     eff_ratios = [eff_ratios; eff_ratio];
% end
% 
% %erod_diffs(erod_diffs>0) = log10(erod_diffs(erod_diffs>0));
% %erod_diffs(erod_diffs<0) = log10(erod_diffs(erod_diffs<0)*-1)*-1;
% %eff_ratios = log(eff_ratios);
% h1 = scatter(eff_ratios,erod_diffs, [], 'filled');
% %set(gca,'yscale', 'log');
% %set(gca,'xscale', 'log');
% hold on;
% 
% effective_runs2 = subdir('../data/effective_jumps/k0.005/*.mat');
% 
% erod_diffs = zeros(length(effective_runs2),1);
% eff_ratios = zeros(length(effective_runs2),1);
% 
% for efn = 1:length(effective_runs2)
%     load(effective_runs2(efn).name);
%     erod_diff = (erodq(ss_end+51))-erodq(ss_end);
%     erod_diffs = [erod_diffs; erod_diff];
%     eff_ratios = [eff_ratios; eff_ratio];
% end
% 
% %erod_diffs(erod_diffs>0) = log10(erod_diffs(erod_diffs>0));
% %erod_diffs(erod_diffs<0) = log10(erod_diffs(erod_diffs<0)*-1)*-1;    
% %eff_ratios = log(eff_ratios);
% h2 = scatter(eff_ratios,erod_diffs, [], 'd', 'filled');
% hold on;


effective_runs3 = subdir('../data/effective_jumps/k0.01/*.mat');

erod_diffs = zeros(length(effective_runs3),1);
eff_ratios = zeros(length(effective_runs3),1);

for efn = 1:length(effective_runs3)
    load(effective_runs3(efn).name);
    erod_diff = (erodq(ss_end+51))-erodq(ss_end);
    erod_diffs = [erod_diffs; erod_diff];
    eff_ratios = [eff_ratios; eff_ratio];
end

%erod_diffs(erod_diffs>0) = log10(erod_diffs(erod_diffs>0));
%erod_diffs(erod_diffs<0) = log10(erod_diffs(erod_diffs<0)*-1)*-1;   
%eff_ratios = log(eff_ratios);

h3 = scatter(eff_ratios,erod_diffs, [], 's', 'filled');

%set(gca,'yscale', 'log');
%set(gca,'xscale', 'log');
%xlim([-100 30000]);

%legend([h1 h2 h3], {'\kappa = 0.001m^{2}yr^{-1}', ...
    %'\kappa = 0.005m^{2}yr{-1}', '\kappa = 0.01m^{2}yr^{-1}'});
ylabel('\Delta m^{2}ka^{-1}');
xlabel('\alpha_1/\alpha_2');

%xincrements = [0.0001 0.001 0.01 0.1 1 10 100 1000];
%xlabels = {'1e-4', '1e-3', '1e-2', '1e-1', '1e0',...
%    '1e1', '1e2', '1e3', '1e4'};
 
%xticks(log(xincrements));
%xticklabels(xincrements);
%yticks(yincrements);
%set(gca, 'yscale', 'log');
set(gca, 'xscale', 'log');
grid on;
print(fig, ['./pdfs' filesep  'effective_jumps.pdf'],'-dpdf')
