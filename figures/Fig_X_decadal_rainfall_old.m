load('../data/decade_series.mat');

figure;
subplot(4,2,1);
s1 = scatter(decade_series_rainfall.wetter.d50(1,:), ...
    decade_series_runoff.wetter.d50(1,:), 'filled');
s1.MarkerFaceAlpha = .2;
hold on;

s2 = scatter(decade_series_rainfall.wetter.d84(1,:), ...
    decade_series_runoff.wetter.d84(1,:), 'filled');
s2.MarkerFaceAlpha = .2;
hold on;

s3 = scatter(decade_series_rainfall.wetter.d99(1,:), ...
    decade_series_runoff.wetter.d99(1,:), 'filled');
s3.MarkerFaceAlpha = .2;

xlabel 'Rainall (mm/day)'
ylabel 'Run off (mm/day)'

title(decade_series_runoff.wetter.station);
grid on;
legend('D_{50}', 'D_{84}', 'D_{99}', 'Location', 'southeast');
set(gca,'XLIM', [0 20]);
set(gca,'YLIM', [0 20]);
hold on;
plot([0 50], [0 50], 'k-');

subplot(4,2,2);
s1 = scatter(decade_series_rainfall.arid.d50(1,:), ...
    decade_series_runoff.arid.d50(1,:), 'filled');
s1.MarkerFaceAlpha = .2;
hold on;

s2 = scatter(decade_series_rainfall.arid.d84(1,:), ...
    decade_series_runoff.arid.d84(1,:), 'filled');
s2.MarkerFaceAlpha = .2;

hold on;
s3 = scatter(decade_series_rainfall.arid.d99(1,:), ...
    decade_series_runoff.arid.d99(1,:), 'filled');
s3.MarkerFaceAlpha = .2;

xlabel 'Rainall (mm/day)'
ylabel 'Run off (mm/day)'

title(decade_series_runoff.arid.station);
grid on;                                                                                                                                                                         
legend('D_{50}', 'D_{84}', 'D_{99}', 'Location', 'southeast');
set(gca,'XLIM', [0 20]);
set(gca,'YLIM', [0 20]);
hold on;
plot([0 50], [0 50], 'k-');

subplot(4,2,3);     

ex_precip = decade_series_runoff.wetter.precip_examples(1,:);
plot(ex_precip)
chance = length(ex_precip(ex_precip>0))/365;
text(20, .6, {['Mean: ' num2str(mean(ex_precip)), ' m/yr'], ...
    ['Chance: ' num2str(chance)]});
ylim([0 0.8]);
xlim([1 365]);
% c1 = cdfplot(decade_series_runoff.wetter.d50(1,:));
% set(c1, 'LineWidth', 1);
% hold on;
% 
% c2 = cdfplot(decade_series_runoff.wetter.d84(1,:));
% set(c2, 'LineWidth', 1);
% 
% hold on;
% c3 = cdfplot(decade_series_runoff.wetter.d99(1,:));
% set(c3, 'LineWidth', 1);
% 
% xlim([0 40]);
% title(decade_series_runoff.wetter.station);
% xlabel 'Run off (mm/day)'
% grid on;
% legend('D_{50}', 'D_{84}', 'D_{99}', 'Location', 'southeast');

subplot(4,2,4);
ex_precip = decade_series_runoff.arid.precip_examples(1,:);
plot(ex_precip)
chance = length(ex_precip(ex_precip>0))/365;
text(20, .6, {['Mean: ' num2str(mean(ex_precip)), ' m/yr'], ...
    ['Chance: ' num2str(chance)]});
ylim([0 0.8]);
xlim([1 365]);

% c1 = cdfplot(decade_series_runoff.arid.d50(1,:));
% set(c1, 'LineWidth', 1);
% hold on;
% 
% c2 = cdfplot(decade_series_runoff.arid.d84(1,:));
% set(c2, 'LineWidth', 1);
% 
% hold on;
% c3 = cdfplot(decade_series_runoff.arid.d99(1,:));
% set(c3, 'LineWidth', 1);
% xlim([0 40]);
% xlabel 'Run off (mm/day)'
% title(decade_series_runoff.arid.station);
% grid on;
% legend('D_{50}', 'D_{84}', 'D_{99}', 'Location', 'southeast');


subplot(4,2,5);
events_flatten = reshape(decade_series_rainfall.wetter.events.',1,[]);
mean_flatten = reshape(decade_series_rainfall.wetter.mean.',1,[]);
max_events_flatten = reshape(decade_series_rainfall.wetter.max_events.',1,[]);

s1 = scatter(mean_flatten, events_flatten, 'filled');
s1.MarkerFaceAlpha = .2;
hold on;

ylim([0 150]);
xlim([0 1]);
ylabel('Days with rain');

yyaxis right;
s2 = scatter(mean_flatten, max_events_flatten, 'filled');
s2.MarkerFaceAlpha = .2;
ylim([0 5]);
xlim([0 0.05]);
ylabel('Rainfall (mm/day)');
xlabel('MAP (mm)');
legend('Rainfall days per year', 'D_{99} event per year');
grid on;

subplot(4,2,6);

events_flatten = reshape(decade_series_rainfall.arid.events.',1,[]);
mean_flatten = reshape(decade_series_rainfall.arid.mean.',1,[]);
max_events_flatten = reshape(decade_series_rainfall.arid.max_events.',1,[]);

s1 = scatter(mean_flatten, events_flatten, 'filled');
s1.MarkerFaceAlpha = .2;
hold on;
ylim([0 150]);
xlim([0 0.05]);
yyaxis right;

s2 = scatter(mean_flatten, max_events_flatten, 'filled');
s2.MarkerFaceAlpha = .2;
ylim([0 5]);
xlim([0 0.05]);
ylabel('Rainfall (mm/day)');
xlabel('MAP (mm)');
legend('Rainfall events per year', 'D_{99} event per year');
grid on;

subplot(4,2,7);
variance_flatten = reshape(decade_series_rainfall.wetter.variance.',1,[]);
mean_flatten = reshape(decade_series_rainfall.wetter.mean.',1,[]);
s1 = scatter(mean_flatten, variance_flatten, 'filled');
s1.MarkerFaceAlpha = 1;
ylim([0 5]);
xlim([0 0.05]);
grid on;
xlabel 'MAP (mm)';
ylabel '\sigma^2';

subplot(4,2,8);
variance_flatten = reshape(decade_series_rainfall.arid.variance.',1,[]);
mean_flatten = reshape(decade_series_rainfall.arid.mean.',1,[]);
s1 = scatter(mean_flatten, variance_flatten, 'filled');
s1.MarkerFaceAlpha = 1;
% ylim([0 1]);
xlim([0 0.05]);
grid on;
xlabel 'MAP (mm)';
ylabel '\sigma^2';

% figure;
% filename = '../data/inyo_stations.csv';
% delimiter = ',';
% startRow = 2;
% formatSpec = '%q%f%f%f%f%f%f%f%f%f%[^\n\r]';
% fileID = fopen(filename,'r');
% dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string', 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
% fclose(fileID);
% inyostations = table(dataArray{1:end-1}, 'VariableNames', {'stations','latitude','longitude','elevation','mean_rainfall','variance','mu','sigma','stdevs','chance_list'});
% clearvars filename delimiter startRow formatSpec fileID dataArray ans;
% 
% mean_rainfall = inyostations.mean_rainfall;
% chance_of_rain = inyostations.chance_list;
% 
% [xData, yData] = prepareCurveData( mean_rainfall, chance_of_rain );
% 
% % Set up fittype and options.
% ft = fittype( 'poly1' );
% 
% % Fit model to data.
% [fitresult, gof] = fit( xData, yData, ft );
% 
% % Plot fit with data.
% h = plot( fitresult, xData, yData );
% legend( h, 'chance_of_rain vs. mean_rainfall', 'untitled fit 1', 'Location', 'NorthEast' );
% % Label axes
% xlabel mean_rainfall
% ylabel chance_of_rain
% grid on
% 

