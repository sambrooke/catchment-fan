
% inyo_names = unique(InyoCounty.NAME);
% inyo_names = inyo_names(strcmp(inyo_names,'NAME') < 1);
% 
% variance = zeros(1, length(inyo_names));
% thresh_1_all = struct('m01',zeros(1, length(inyo_names)), 'm02', zeros(1, length(inyo_names)),...
%     'm03',zeros(1, length(inyo_names)));
% thresh_2_all = struct('m01',zeros(1, length(inyo_names)), 'm02', zeros(1, length(inyo_names)),...
%     'm03',zeros(1, length(inyo_names)));
% thresh_3_all = struct('m01',zeros(1, length(inyo_names)), 'm02', zeros(1, length(inyo_names)),...
%     'm03',zeros(1, length(inyo_names)));
% 
% years = 1000;
% 
% for inyo_n = 1:length(inyo_names)
%     station_name = inyo_names{inyo_n};
%     rain = InyoCounty(InyoCounty.NAME==station_name,:);
%     no_negs = rain.PRCP>-1;
%     precip = rain.PRCP(no_negs)/1000;
%     dates = rain.DATE(no_negs);
%     dates = datetime(dates, 'ConvertFrom','yyyymmdd');
%     no_precip = precip(precip<=0);
%     precip = precip(precip>0);
%     pd = fitdist(precip, 'lognormal');
%     chance = length(precip)/length(no_precip);
%     edges = 0:.01:2;
%     alpha_dist = zeros(years,1);
%     
%     thousand_year_rainfall = [];
%     
%     % Generate mean conserved PDF for 1000 year daiy rainfall series
%     
%     for y=1:years
%         precip = zeros(365,1);
% 
%         for d=1:length(precip)
%             if rand <= chance
%                 precip(d) = random(pd);
%             end
%         end
%             
%         thousand_year_rainfall = [thousand_year_rainfall; precip];
%     end
%     
%     mean_targets = [0.1 0.2 0.3];
%     mean_targets_labels = {'m01', 'm02', 'm03'};
%         
%     for mt = 1:length(mean_targets)
% 
%         mean_target = mean_targets(mt);
%         alpha_dist_norm = thousand_year_rainfall/max(thousand_year_rainfall);
%         mean_orig = mean(alpha_dist_norm);
%         alpha_mean_con = alpha_dist_norm*(mean_target/mean_orig);
% 
%         variance(inyo_n) = std(alpha_mean_con)^2;
% 
%         iterate = 1;
%         tolerance = 0.01;
%         area = trapz(alpha_mean_con);
% 
%         ef_dist_1 = zeros(years,1);
%         ef_dist_2 = zeros(years,1);
%         ef_dist_3 = zeros(years,1);
%         
%         % New mean-conserving PDF
%         no_precip = alpha_mean_con(alpha_mean_con<=0);
%         precip = alpha_mean_con(alpha_mean_con>0);
%         pd = fitdist(precip, 'lognormal');
%         chance = length(precip)/length(no_precip);
%     
%         for y=1:years
% 
%             precip_empty = 1;
%             
%             while precip_empty 
%                 precip = zeros(365,1);
%                 
%                 for d=1:length(precip)
%                     if rand <= chance
%                         precip(d) = random(pd);
%                     end
%                 end
%             
%                 precip(isnan(precip)) = 0;
%                 if trapz(precip) > 0
%                     precip_empty = 0;
%                 end
%             end
% 
%             eF_list_1 = infiltration(precip, 0.5, 0.5);
%             eF_list_2 = infiltration(precip, 1, 0.5);
%             eF_list_3 = infiltration(precip, 2, 0.5);
%             ef_dist_1(y) = mean(eF_list_1);
%             ef_dist_2(y) = mean(eF_list_2);
%             ef_dist_3(y) = mean(eF_list_3);
%         end
% 
%         incident_mean = mean(alpha_mean_con);
%         thresh_1_all.(mean_targets_labels{mt})(inyo_n) = mean(ef_dist_1)/incident_mean;
%         thresh_2_all.(mean_targets_labels{mt})(inyo_n) = mean(ef_dist_2)/incident_mean;
%         thresh_3_all.(mean_targets_labels{mt})(inyo_n) = mean(ef_dist_3)/incident_mean;
%     end
% end
% 
% thresh_1_struct = struct('m01',[],'m02',[],'m03',[]);
% thresh_2_struct = struct('m01',[],'m02',[],'m03',[]);
% thresh_3_struct = struct('m01',[],'m02',[],'m03',[]);
% 
% thresh_1_struct.('m01') = thresh_1_all.('m01')*100;
% thresh_2_struct.('m01') = thresh_2_all.('m01')*100;
% thresh_3_struct.('m01') = thresh_3_all.('m01')*100;
% 
% thresh_1_struct.('m02') = thresh_1_all.('m02')*100;
% thresh_2_struct.('m02') = thresh_2_all.('m02')*100;
% thresh_3_struct.('m02') = thresh_3_all.('m02')*100;
% 
% thresh_1_struct.('m03') = thresh_1_all.('m03')*100;
% thresh_2_struct.('m03') = thresh_2_all.('m03')*100;
% thresh_3_struct.('m03') = thresh_3_all.('m03')*100;


X = 25;
Y = 8;                  
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# fig53ure size on paper (width & height)
ySize = Y - 2*yMargin;     %# fig53ure size on paper (width & height)
% Start at which timestep

fig5 = figure();
set(fig5, 'PaperSize',[X Y]);
set(fig5, 'PaperPosition',[0 yMargin xSize ySize])
set(fig5, 'PaperUnits','centimeters');

x_inc = 0:0.01:20;
fnames = {'m01', 'm02', 'm03'};
titles = {'Mean = 0.1m/yr', 'Mean = 0.2m/yr', 'Mean = 0.3m/yr'};

cm = winter(3);

for fn = 1:length(fnames)
    
    thresh_1 = thresh_1_struct.(fnames{fn});
    thresh_2 = thresh_2_struct.(fnames{fn});
    thresh_3 = thresh_3_struct.(fnames{fn});
    
    subplot(1,3,fn);
    [xData, yData] = prepareCurveData(variance, thresh_1);
    % Set up fittype and options.
    ft = fittype('exp2');
    opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
    opts.Display = 'Off';
    % Fit model to data.
    [fd, gof] = fit( xData, yData, ft, opts );

    y_fit = fd.a.*exp(fd.b.*x_inc) + fd.c.*exp(fd.d.*x_inc);

    % Plot fit with data.
    h = scatter(xData, yData, 'filled', 'MarkerEdgeColor', ...
            cm(1,:), 'MarkerFaceColor', cm(1,:));
    hold on;
    h1 = plot(x_inc, y_fit, '-', 'Color', cm(1,:));

    hold on;

    [xData, yData] = prepareCurveData(variance, thresh_2);
    % Set up fittype and options.
    ft = fittype('exp2');
    opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
    opts.Display = 'Off';
    % Fit model to data.
    [fd, gof] = fit( xData, yData, ft, opts );

    y_fit = fd.a.*exp(fd.b.*x_inc) + fd.c.*exp(fd.d.*x_inc);

    % Plot fit with data.
    h = scatter(xData, yData, 'filled', 'MarkerEdgeColor', ...
            cm(2,:), 'MarkerFaceColor', cm(2,:));
    hold on; 
    h2 = plot(x_inc, y_fit, '-', 'Color', cm(2,:));

    hold on;

    [xData, yData] = prepareCurveData(variance, thresh_3);
    % Set up fittype and options.
    ft = fittype('exp2');
    opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
    opts.Display = 'Off';
    
    % Fit model to data.
    [fitresult, gof] = fit( xData, yData, ft, opts );
    % Plot fit with data.
    [fd, gof] = fit( xData, yData, ft, opts );

    y_fit = fd.a.*exp(fd.b.*x_inc) + fd.c.*exp(fd.d.*x_inc);

    % Plot fit with data.
    h = scatter(xData, yData, 'filled', 'MarkerEdgeColor', ...
            cm(3,:), 'MarkerFaceColor', cm(3,:));
    hold on;
    h3 = plot(x_inc, y_fit, '-', 'Color', cm(3,:));
    title(titles{fn});
    ylim([0 100]);
%     xlim([0.001 1]);
%     set(gca,'Xscale', 'log');
    xlabel('\sigma^2');
    ylabel('% Effective \alpha');
    grid on;
    
    lp = [h1, h2, h3];
    legend(lp,{'0.5m', '1m', '2m'}, 'Location', 'southeast');

end

fig5.Renderer='Painters';
print(fig5, ['./pdfs' filesep  'infiltration_percent.pdf'],'-dpdf');
