
filename = '../data/step_data.csv';
delimiter = ',';
startRow = 2;

formatSpec = '%f%f%f%f%f%f%f%f%f%f%f%f%[^\n\r]';

fileID = fopen(filename,'r');

dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string', 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');

fclose(fileID);

stepdata14new = table(dataArray{1:end-1}, 'VariableNames', {'a1s','a2s','v1s','v2s','vjumps','dists_20','dists_30','dists_40','stds_20','stds_30','stds_40','sats'});

clearvars filename delimiter startRow formatSpec fileID dataArray ans;
clc;
addpath('../');
X = 20;
Y = 15;                  
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# fig3ure size on paper (width & height)
ySize = Y - 2*yMargin;     %# fig3ure size on paper (width & height)

fig3 = figure();

set(fig3, 'PaperSize',[X Y]);
set(fig3, 'PaperPosition',[0 yMargin xSize ySize])
set(fig3, 'PaperUnits','centimeters');

stepdata = stepdata14new;

step_data_0 = stepdata(stepdata.sats==0,:);
step_data_0_1 = stepdata(stepdata.sats==0.2,:);
step_data_0_2 = stepdata(stepdata.sats==0.5,:);
step_data_0_3 = stepdata(stepdata.sats==1,:);

datasets = {step_data_0_1, step_data_0_2, step_data_0_3};
subplot_pos = [1,4,2,5,3,6];

[ha, pos] = tight_subplot(2, 3, [0.15,0.05], [0.1 0.05], [0.1 0.05]);
ha(1).ActivePositionProperty = 'outerposition';
ha(2).ActivePositionProperty = 'outerposition';
ha(3).ActivePositionProperty = 'outerposition';
ha(4).ActivePositionProperty = 'outerposition';
ha(5).ActivePositionProperty = 'outerposition';
ha(6).ActivePositionProperty = 'outerposition';

caxis('manual');

colormap bone

for sdn = 1:3
    disp(sdn)
    v1s = datasets{sdn}.v1s;
    v2s = datasets{sdn}.v2s;
    distsd = datasets{sdn}.dists_20;
    
    [xData, yData, zData] = prepareSurfaceData( v1s, v2s, distsd );

    % Set up fittype and options.
    ft = fittype( 'loess' );
    opts = fitoptions( 'Method', 'LowessFit' );
    opts.Normalize = 'on';
    opts.Robust = 'LAR';
    opts.Span = 0.9;
    
    % Fit model to data.
    [fitresult, gof] = fit( [xData, yData], zData, ft, opts );


    % Create a figure for the plots.
    % Plot fit with data.
    ax1 = ha(sdn);
    axes(ax1);
    
    
    h = plot( fitresult, [xData, yData], zData );
    surf = h(1);
    set(surf,'FaceAlpha',0.8);
    surf.EdgeColor = 'none';
    
    caxis(ax1, [-2000 2000]);
    % Label axes
    xlabel('\sigma^{2} 1');
    ylabel('\sigma^{2} 2');
    zlabel('Distance (m)')
    grid on
    zlim([-2000,2000]);
    zticks([-2000 -1500 -1000 -500 0 500 1000 1500 2000]);
    view( 48.5, 26.0 );

    % Make contour plot.
    ax2 = ha(sdn+3);
    axes(ax2);
    h = plot(fitresult, 'Style', 'Contour');
    
    
% 
%     if sdn == 1
%         set(h, 'LevelList', [-1600,-1400,-1200,-1000,-800,-600,-400,-200,-100,-50,-25,0,10,25,50,100,200,400,600,800,1000,1200,1400,1600]);
%     else
%         set(h, 'LevelList', [-1600,-1400,-1200,-1000,-800,-600,-400,-200,-100,-50,0,50,100,200,400,600,800,1000,1200,1400,1600]);
%     end
    set(h, 'LevelList', [-2000:200:2000]);
    
    set(h, 'ShowText', 'on');

    caxis(ax2, [-2000 2000]);
    
    xticks([0 0.5 1 1.5]);
    yticks([0 0.5 1 1.5]);
    % Label axes
    xlabel('\sigma^{2} 1');
    ylabel('\sigma^{2} 2');

    grid on
    
end
fig3.Renderer = 'Painters';
plot_destination = './pdfs';
print(fig3, [plot_destination filesep 'step_ranges.pdf'],'-dpdf');