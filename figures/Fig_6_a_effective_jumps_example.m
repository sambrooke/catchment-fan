[kappa,c,nexp,lx,u0,alpha_realstep,C1,ss_yield] = reset_params
kappa = 0.005;
u0 = 0.001;
c = 0.01;
alpha = [ones(1,50)*1, ones(1,50)*2];
ss_alpha_condition = alpha(1);
tmax = 200;

fan_evo;

X = 15;
Y = 5;
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# figure size on paper (width & height)
ySize = Y - 2*yMargin;     %# figure size on paper (width & height)
% Start at which timestep
plot_t_start = ss_end+1;

fig = figure();

set(fig,'visible','off');
set(fig, 'PaperSize',[X Y]);
set(fig, 'PaperPosition',[0 yMargin xSize ySize])
set(fig, 'PaperUnits','centimeters');

plot(erodq(ss_end+1:end));
ylabel('qs (m^{2}ka^{-1})');
xlim([0 100]);
ylim([.5 1.5]);
yyaxis right;
stairs(alpha);
ylabel('$\bar{\alpha}$ (m)', 'Interpreter', 'latex');
ylim([-8 3]);
yticks([0 1 2 3 4]);
yticklabels([0 1 2 3 4]);
hold on;
yyaxis left;
plot([50 51],erodq(ss_end+50:ss_end+51), 'o');
xlabel('Time (ka)');
plot_destination = './pdfs';
run_title = 'effective_jump_example';
print(fig, [plot_destination filesep run_title '.pdf'],'-dpdf')



