% Example plot to illustrate how we manupulate rainfall time
% to produce changing distirbutions with the same MAP
% 'LAKE SABRINA, CA US' vs 'GREENLAND RANCH, CA US'

X = 15;
Y = 20; 
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# figure size on paper (width & height)
ySize = Y - 2*yMargin;     %# figure size on paper (width & height)
% Start at which timestep

fig = figure();

set(fig,'visible','off');
set(fig, 'PaperSize',[X Y]);
set(fig, 'PaperPosition',[0 yMargin xSize ySize])
set(fig, 'PaperUnits','centimeters');

% CHANGE TO LAKE SABRINA FOR COUNTERPART FIGURE
rain = InyoCounty(strcmp(InyoCounty.NAME, 'GREENLAND RANCH, CA US'),:);
no_negs = rain.PRCP>-1;
precip = rain.PRCP(no_negs)/1000; % Convert to metres/day
no_precip = precip(precip<=0);
precip = precip(precip>0);
pd = fitdist(precip, 'lognormal');
chance = length(precip)/length(no_precip);

years = 1000;

original_MAP = zeros(years,1);
original_precip = [];

for y=1:years

    precip = zeros(365,1);

    for d=1:length(precip)
        if rand <= chance
            precip(d) = random(pd);
        end
    end
    
    original_precip = [original_precip;precip];
    original_MAP(y) = mean(precip);
end

mean_target = 0.1;
alpha_dist_norm = original_MAP/max(original_MAP);
mean_orig = mean(alpha_dist_norm);
alpha_mean_con = alpha_dist_norm*(mean_target/mean_orig);

total_precip_days = [];

fixed_MAP = zeros(years,1);

for y=1:years

    precip_empty = 1;

    while precip_empty 
        precip = zeros(365,1);

        for d=1:length(precip)
            if rand <= chance
                precip(d) = random(pd);
            end
        end

        precip(isnan(precip)) = 0;
        if trapz(precip) > 0
            precip_empty = 0;
        end
    end

    mean_target = alpha_mean_con(y);
    precip_norm = precip/max(precip);
    mean_orig = mean(precip_norm);
    precip_mean_con = precip_norm*(mean_target/mean_orig);
    fixed_MAP(y) = mean(precip_mean_con);
    total_precip_days = [total_precip_days; precip_mean_con];
end

subplot(2,2,1);

plot(original_MAP);
ylabel('MAP (m)');
xlabel('Time (yrs');
textLoc(['\bf Mean Raifall = ', num2str(mean(original_precip)*1000), 'mm'], 'northeast');
title('Original Mean MAP');
ylim([0,0.003])
subplot(2,2,2);

plot(fixed_MAP);
ylabel('MAP (m)');
xlabel('Time (yrs');
textLoc(['\bf Mean Raifall = ', num2str(mean(total_precip_days)*1000), 'mm'], 'northeast');
title('Fixed Mean MAP');
ylim([0,0.5]);
subplot(2,2,3);
edges = 0:.001:.1;

no_precip = original_precip(original_precip<=0);
precip = original_precip(original_precip>0);
chance = length(precip)/length(no_precip);

pdalpha = fitdist(original_precip(original_precip>0), 'lognormal');
dist_pdf = pdf(pdalpha, edges);
dist_pdf_norm = dist_pdf./trapz(dist_pdf);
plot(edges,dist_pdf_norm, 'LineWidth',1);
textLoc(['\bf Chance of rain = ' num2str(chance)], 'northeast');
title('PDF Daily Rain Event');
xlabel('Precipitation (m)');
ylim([0,0.4]);
subplot(2,2,4);
edges = 0:.001:10;

no_precip = total_precip_days(total_precip_days<=0);
precip = total_precip_days(total_precip_days>0);
chance = length(precip)/length(no_precip);

pdalpha = fitdist(total_precip_days(total_precip_days>0), 'lognormal');
dist_pdf = pdf(pdalpha, edges);
dist_pdf_norm = dist_pdf./trapz(dist_pdf);
plot(edges,dist_pdf_norm, 'LineWidth',1);
textLoc(['\bf Chance of rain = ' num2str(chance)], 'northeast');
title('PDF Daily Rain Event');
xlabel('Precipitation (m)');
ylim([0,0.004])

plot_destination = './pdfs';
print(fig, [plot_destination filesep 'dist_change2.pdf'],'-dpdf');
