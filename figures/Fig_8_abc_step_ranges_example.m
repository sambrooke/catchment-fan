addpath('../model');
addpath('../processing');

load('../data/inyo_time_series.mat');

% 3 - lower variance
% 20 - high variance

reps = 5;

incident_A = repmat(inyo_time_series_incident(3,:),1, reps);
incident_B = repmat(inyo_time_series_incident(20,:),1, reps);

alpha_A = repmat(inyo_time_series_t1(3,:),1, reps);
alpha_B = repmat(inyo_time_series_t1(20,:),1, reps);

variance_A = repmat(inyo_times_series_variance(3,:),1, reps);
variance_B = repmat(inyo_times_series_variance(20,:),1, reps);

alpha_events_A = repmat(inyo_time_series_event_props(3,:),1, reps);
alpha_events_B = repmat(inyo_time_series_event_props(20,:),1, reps);

%alpha_all = [repmat(alpha_A,1,10)';repmat(alpha_B,1,10)'];
alpha = [alpha_A, alpha_B];
variance_all = [variance_A, variance_B];
alpha_events_prop = [alpha_events_A, alpha_events_B];
incident = [incident_A, incident_B];

[kappa,c,nexp,lx,u0,alpha_realstep,C1,ss_yield] = reset_params;
kappa = 0.005;
tmax = length(alpha);
ss_alpha_condition = mean(alpha_A);

fan_evo;

X = 15;
Y = 25;                  
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# fig3ure size on paper (width & height)
ySize = Y - 2*yMargin;     %# fig3ure size on paper (width & height)

grainpdf_mm = exp(grainpdf');

plot(erodq);
hold on;
yyaxis right
ylim([0 2]);
yticks([0:0.5:2]);
yticklabels([0:0.5:2]);

fig = figure();

set(fig,'visible','off');
set(fig, 'PaperSize',[X Y]);
set(fig, 'PaperPosition',[0 yMargin xSize ySize])
set(fig, 'PaperUnits','centimeters');

plot_t_start = ss_end+10;
subplot(4,1,1);
p0 = plot(variance_all, 'k-', 'LineWidth', 1);
yticks([0:1:3]);
yticklabels([0:1:3]);
ylim([0 5]);
ylabel('\sigma^2');
yyaxis right
p1 = plot(alpha, 'r-');
hold on
p2 = plot(incident, 'b-');
yticks([0:0.05:0.15]);
yticklabels([0:0.05:0.15]);
ylim([-0.1 0.2]);
ylabel('\alpha');
xticks([0:100:1000]);
xticklabels([0:0.1:1]);
xlabel('Time (Ma)');
legend('\sigma^2', 'Effective \alpha', 'Incident \alpha',...
    'location', 'northwest', 'orientation', 'horizontal');

subplot(4,1,2);
plot(erodq(plot_t_start:end), '-');
ylabel('qs (m^{2}ka^{-1})');
xticks([0:100:1000]);
xticklabels([0:0.1:1]);
xlabel('Time (Ma)');

subplot(4,1,3:4);

% Using natural log so we remake grain size with exp()
max_size = max(max(grainpdf_mm(plot_t_start:end, :)));
max_size = ceil(max_size);
max_size = max(max(floor(grainpdf_mm/10)))*10;
max_size = 200;
cm = jet(max_size);
colormap(cm);

ss_realstep = 1e6;
ss_dt = get_dt(kappa, ss_realstep, 1, lx);

sg = size(grainpdf_mm);
% t_end = fs_y;
% x_end = fs_x;
t_end = sg(1);
x_end = sg(2);

% Y step size

t_step = 5;
t_start = plot_t_start;

% X step size
dx = 5;

% Y step size

t_start = plot_t_start;

fan_heights = fan_elevation;
fan_x = coord_basin*lx;
fan_dx = fan_x(x_end)/max(x_end);
ss_aquisition = plot_t_start;

alpha_x = 1:1:length(alpha);
alpha_x = alpha_x.*alpha_realstep./1e3;
alpha_x = alpha_x((plot_t_start):end);

maxPrecip = max(alpha);
maxErosion = max(erodq(t_start:end));

for y=t_start:t_step:t_end

    y_b = y;
    y_t = y+t_step;
    if y_t > t_end
        y_t = t_end;
    end
    
    for x=1:dx:x_end
        x_l = x;
        x_r = x+dx;
       
        if x_r > x_end
            x_r = x_end;
        end
        
        % SWAPPED fan_elevation(t,:) to fan_elevation(:,t)
        xco = [fan_x(x_l) fan_x(x_r) fan_x(x_r) fan_x(x_l)];
        zco = [fan_heights(x, y_t), fan_heights(x_r,y_t), fan_heights(x_r,y_b), fan_heights(x,y_b)];
        
        colourID = mean(mean(grainpdf_mm(y_b:y_t,x_l:x_r),'omitnan'),'omitnan');
        %colourID = max(max(grainpdf_mm(y_b:y_t,x_l:x_r)));
        
        if isnan(colourID)
            colour = [1 1 1];
        else
            color_ceil = ceil(colourID);
            if color_ceil > max_size
                colour = cm(end,:);
            else
                colour = cm(color_ceil,:);
            end
        end
        
        if sum(colour) == 0
           disp('No colour!');
        end
        h = patch(xco, zco, colour, 'EdgeColor', 'none');
        drawnow
    end
    
end

%Time Slices (thousands of years)

% ka_slices = [200 400 600 800 1000 1200 1400 1600 1800 2000];
% for kslice = 1:length(ka_slices)
%     ka_line = fan_heights(:, ka_slices(kslice));
%     line(1:fan_dx:fan_x(x_end), ka_line,'LineWidth',0.5, 'Color', [1 1 1]);    
% end

marker_gs = [20];

for mgs = 1:length(marker_gs)
    x_coords = [];
    y_coords = [];
    for y=ss_end+1:1:t_end
        [cvv index] = min(abs(grainpdf_mm(y,:)-marker_gs(mgs)))

        x_coords = [x_coords; fan_x(index)];
        y_coords = [y_coords; fan_heights(index, y)];
    end
    
    line(x_coords, y_coords,'LineWidth',0.5, 'Color', [0 0 0]);
end

hcb=colorbar;
set(hcb,'YTick', 0:.2:1);
set(hcb,'YTickLabel',linspace(0,ceil(max_size),6));
ylabel(hcb,'Grain Size (mm)') 

xlabel('Metres');
ylabel('Metres');
custom_lines = [t_start, t_end];

for ts = 1:length(custom_lines)
    cs_line = fan_heights(:, custom_lines(ts));
    line(1:fan_dx:fan_x(x_end), cs_line,'LineWidth',1.3, 'Color', [0 0 0]);
end

ylim([-500 ceil(max(fan_heights(:,end)))]);
plot_destination = './pdfs';
run_title = 'step_ranges_example';
fig.Renderer = 'Painters';
print(fig, [plot_destination filesep run_title '.pdf'],'-dpdf');
