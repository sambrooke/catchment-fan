load('../data/inyo_time_series.mat');
sx = size(inyo_time_series_incident);

incident_mean = zeros(1,sx(1));
events_mean = zeros(1,sx(1));
variance_mean = zeros(1,sx(1));

for n = 1:sx(1)
    incident_mean(n) = mean(inyo_time_series_incident(n,:));
    events_mean(n) = mean(inyo_time_series_event_props(n,:));
    variance_mean(n) = mean(inyo_times_series_variance(n,:));
end

[xData, yData] = prepareCurveData( mean_rainfall, chance_of_rain );

% Set up fittype and options.
ft = fittype( 'poly1' );
opts = fitoptions( 'Method', 'LinearLeastSquares' );
opts.Lower = [-Inf 0];
opts.Upper = [Inf 0];

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

% Plot fit with data.
figure( 'Name', 'untitled fit 1' );
h = plot( fitresult, xData, yData );
legend( h, 'chance_of_rain vs. mean_rainfall', 'untitled fit 1', 'Location', 'NorthEast' );
% Label axes
xlabel mean_rainfall
ylabel chance_of_rain
grid on

