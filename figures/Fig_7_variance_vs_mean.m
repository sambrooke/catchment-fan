
fig2 = figure();

X = 15;
Y = 20;
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# fig6ure size on paper (width & height)
ySize = Y - 2*yMargin;     %# fig6ure size on paper (width & height)

set(fig2,'visible','off');
set(fig2, 'PaperSize',[X Y]);
set(fig2, 'PaperPosition',[0 yMargin xSize ySize])
set(fig2, 'PaperUnits','centimeters');

[haa, pos] = tight_subplot(6, 2, [0.02,0.02], [0.3 0.05], [0.1 0.05]);

columns = [[1,3,5,7,9,11];[2,4,6,8,10,12]];
colours = cool(4);

% Set subplot size
for ii = 1:2
    col = columns(ii,:);
    buffer_n = 0.02;
    for jj = 1:6
        
        haa(col(jj)).ActivePositionProperty = 'outerposition';
        if jj <= 4
            current_pos = haa(col(jj)).Position;
            new_pos = [current_pos(1), current_pos(2),...
                current_pos(3), current_pos(4)];
            set(haa(col(jj)), 'Position', new_pos);
        end

        if jj > 4
            current_pos = haa(col(jj)).Position;
            height_change = 0.1;
            position_change = height_change*buffer_n;
            buffer_n = buffer_n+height_change;
            new_pos = [current_pos(1), current_pos(2)-buffer_n,...
                current_pos(3), current_pos(4)+height_change];
            set(haa(col(jj)), 'Position', new_pos);
        end
    end
end

plot_params = struct(...
'ylim_params' , [-1000 500],...
'y_interval', 500,...
't_step' , 5,...
'dx' , 5);

load('../data/null_runs/mean_05m_threshold');
columns = [[1,3,5,7,9,11];[2,4,6,8,10,12]];x

xtick_incs = [0 100 200 300 400 500 600];

% Plot Mean rainfall runs
axes(haa(columns(1,1)));
plot(1:1:plot_len,alpha_incident, 'b', 'LineWidth', 1.2);
hold on;
plot(1:1:plot_len,alpha, 'r', 'LineWidth', 1.2);
hold on;

ylim([0 0.5]);
grid on;
yticks([0 0.1 0.2 0.3 0.4 0.5]);
xticks(xtick_incs);
xticklabels([]);
ylabel('\alpha');
legend('Incident', 'Effective');

axes(haa(columns(1,2)));
plot(1:1:plot_len,rdata.variance, 'k', 'LineWidth', 1);
hold on
plot(1:1:plot_len,rdata.events_prop, 'color', [0.8510 0.3255 0.0980],...
    'LineStyle', '--', 'LineWidth', 1.2);
legend('\sigma^2', 'P(r)');
    
ylim([0 1.5]);
grid on;
yticks([0 0.5 1 1.5]);
xticks(xtick_incs);
xticklabels([]);
ylabel('\sigma^2 / P(r)');

axes(haa(columns(1,3)));
plot(1:1:plot_len,erodq(ss_end:(ss_end-1+plot_len)), 'r', 'LineWidth', 1.2);
hold on;

axes(haa(columns(1,4)));
plot(1:1:plot_len,tbs(ss_end:(ss_end-1+plot_len)), 'm', 'LineWidth', 1.2);
hold on;

% Plot fan strat
axes(haa(columns(1,6)));
grainpdf_mm = exp(grainpdf');
max_size = max(max(floor(grainpdf_mm/10)))*10;
max_size = 50;
patch_plot_nofig;  
ylabel('Elevation (m)');
xlabel('Distance (m)');
title([]);

load('../data/null_runs/variance_05m_threshold');
columns = [[1,3,5,7,9,11];[2,4,6,8,10,12]];

% Plot Variance rainfall runs
axes(haa(columns(2,1)));
plot(1:1:plot_len,alpha_incident, 'b', 'LineWidth', 1.2);
hold on;
plot(1:1:plot_len,alpha, 'r', 'LineWidth', 1.2);
hold on;

ylim([0 0.5]);
grid on;
yticks([0 0.1 0.2 0.3 0.4 0.5]);
xticks(xtick_incs);
xticklabels([]);
yticklabels([]);
legend('Incident', 'Effective');

axes(haa(columns(2,2)));
plot(1:1:plot_len,rdata.variance, 'k', 'LineWidth', 1);
hold on
plot(1:1:plot_len,rdata.events_prop, 'color', [0.8510 0.3255 0.0980],...
    'LineStyle', '--', 'LineWidth', 1.2);
legend('\sigma^2', 'P(r)');

ylim([0 1.5]);
grid on;
yticks([0 0.5 1 1.5]);
xticks(xtick_incs);
yticklabels([]);
xticklabels([]);

axes(haa(columns(2,3)));
plot(1:1:plot_len,erodq(ss_end:(ss_end-1+plot_len)), 'r', 'LineWidth', 1.2);
hold on;

axes(haa(columns(2,4)));
plot(1:1:plot_len,tbs(ss_end:(ss_end-1+plot_len)), 'm', 'LineWidth', 1.2);
hold on;

% Plot fan strat
axes(haa(columns(2,6)));
grainpdf_mm = exp(grainpdf');
max_size = max(max(floor(grainpdf_mm/10)))*10;
max_size = 100;
patch_plot_nofig;  
xlabel('Distance (m)');
ylabel([]);
yticklabels([]);
title([]);

load('../data/null_runs/mean_no_threshold');
columns = [[1,3,5,7,9,11];[2,4,6,8,10,12]];

axes(haa(columns(1,5)));
grainpdf_mm = exp(grainpdf');
max_size = max(max(floor(grainpdf_mm/10)))*10;
max_size = 50;
patch_plot_nofig;  
ylabel('Distance (m)');
xticklabels([]);
xlabel([]);
title([]);

axes(haa(columns(1,3)));
plot(1:1:plot_len,erodq(ss_end:(ss_end-1+plot_len)), 'b', 'LineWidth', 1.2);
ylim([0 8]);
grid on;
yticks([0 2 4 6 8]);
ylabel('qs m^2yr^-1');
xticks(xtick_incs);

xticklabels([]);
xlabel([]);
title([]);
legend('0.5 m', '0 m');

axes(haa(columns(1,4)));
plot(1:1:plot_len,tbs(ss_end:(ss_end-1+plot_len)), 'k', 'LineWidth', 1.2);
ylim([0 80]);
xticks(xtick_incs);
yticks([0 20 40 60 80]);
yticklabels([0 20 40 60 80]);
grid on;
xlabel('ka');
ylabel('\tau_b');
legend('0.5 m', '0 m');

load('../data/null_runs/variance_no_threshold');
columns = [[1,3,5,7,9,11];[2,4,6,8,10,12]];

axes(haa(columns(2,5)));
grainpdf_mm = exp(grainpdf');
max_size = max(max(floor(grainpdf_mm/10)))*10;
max_size = 50;

patch_plot_nofig;  
yticklabels([]);
xticklabels([]);
ylabel([]);
xlabel([]);
title([]);

axes(haa(columns(2,3)));
plot(1:1:plot_len,erodq(ss_end:(ss_end-1+plot_len)), 'b', 'LineWidth', 1.2);
hold on;
ylim([0 6]);
grid on;
yticks([0 2 4 6]);
xticks(xtick_incs);
xticklabels([]);
yticklabels([]);
xlabel([]);
title([]);
legend('0.5 m', '0 m');

axes(haa(columns(2,4)));
plot(1:1:plot_len,tbs(ss_end:(ss_end-1+plot_len)), 'k', 'LineWidth', 1.2);
ylim([0 80]);
yticks([0 20 40 60 80]);
xticks(xtick_incs);
grid on;
xlabel('ka');
yticklabels([]);
ylabel([]);
legend('0.5 m', '0 m');

fig2.Renderer = 'Painters';
plot_destination = './pdfs';
print(fig2, [plot_destination filesep 'fig_7_variance_v_mean.pdf'],'-dpdf');

for j = 1:length(haa)
    set(haa(j), 'FontSize', 14);
end
