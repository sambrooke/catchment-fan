clear;
clc;
addpath('../');
runs = subdir('./threshold_pulse_data/100_ka_runs/*.mat');

figure;
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alpha_variances.t0)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alpha_variances.t0(1:phase_length);
   pl = plot(time_x,alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0.5 1]);
xlabel('Ma');
ylabel('\sigma^2');
legend(v_label_items,v_labels);
title('Alpha Variances');

figure;
subplot(2,3,1);
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alphas.t0)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alphas.t0(1:phase_length);
   pl = plot(time_x,alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0 1.5]);
xlabel('Ma');
ylabel('\alpha');
title('Threshold = 0');

subplot(2,3,2);
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alphas.t0_1)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alphas.t0_1(1:phase_length);
   pl = plot(time_x, alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0 1.5]);
xlabel('Ma');
ylabel('\alpha');title('Threshold = 0.1');

subplot(2,3,3);
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alphas.t0_2)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alphas.t0_2(1:phase_length);
   alpha_thr(alpha_thr==0) = NaN;
   pl = plot(time_x, alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0 1.5]);
xlabel('Ma');
ylabel('\alpha');title('Threshold = 0.2');

subplot(2,3,4);
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alphas.t0_3)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alphas.t0_3(1:phase_length);
   alpha_thr(alpha_thr==0) = NaN;
   pl = plot(time_x, alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0 1.5]);
xlabel('Ma');
ylabel('\alpha');title('Threshold = 0.3');

subplot(2,3,5);
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alphas.t0_4)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alphas.t0_4(1:phase_length);
   alpha_thr(alpha_thr==0) = NaN;
   pl = plot(time_x, alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0 1.5]);
xlabel('Ma');
ylabel('\alpha');title('Threshold = 0.4');

subplot(2,3,6);
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alphas.t0_5)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alphas.t0_5(1:phase_length);
   alpha_thr(alpha_thr==0) = NaN;
   pl = plot(time_x, alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0 1.5]);
xlabel('Ma');
ylabel('\alpha');title('Threshold = 0.5');

figure;
subplot(2,3,1);
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alphas.t0)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alphas.t0(1:phase_length);
   pl = plot(time_x,alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0 1.5]);
xlabel('Ma');
ylabel('\alpha');
title('Threshold = 0');

subplot(2,3,2);
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alphas.t0_1)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alphas.t0_1(1:phase_length);
   pl = plot(time_x, alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0 1.5]);
xlabel('Ma');
ylabel('\alpha');
title('Threshold = 0.1');

subplot(2,3,3);
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alphas.t0_2)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alphas.t0_2(1:phase_length);
   alpha_thr(alpha_thr==0) = NaN;
   pl = plot(time_x, alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0 1.5]);
xlabel('Ma');
ylabel('\alpha');
title('Threshold = 0.2');

subplot(2,3,4);
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alphas.t0_3)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alphas.t0_3(1:phase_length);
   alpha_thr(alpha_thr==0) = NaN;
   pl = plot(time_x, alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0 1.5]);
xlabel('Ma');
ylabel('\alpha');
title('Threshold = 0.3');

subplot(2,3,5);
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alphas.t0_4)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alphas.t0_4(1:phase_length);
   alpha_thr(alpha_thr==0) = NaN;
   pl = plot(time_x, alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0 1.5]);
xlabel('Ma');
ylabel('\alpha');
title('Threshold = 0.4');

subplot(2,3,6);
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alphas.t0_5)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alphas.t0_5(1:phase_length);
   alpha_thr(alpha_thr==0) = NaN;
   pl = plot(time_x, alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0 1.5]);
xlabel('Ma');
ylabel('\alpha');
title('Threshold = 0.5');


figure;
subplot(2,3,1);
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alpha_yield.t0)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alpha_yield.t0(1:phase_length);
   pl = plot(time_x,alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0 1.1]);
xlabel('Ma');
ylabel('Yield');
title('Threshold = 0');

subplot(2,3,2);
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alpha_yield.t0_1)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alpha_yield.t0_1(1:phase_length);
   pl = plot(time_x, alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0 1.1]);
xlabel('Ma');
ylabel('Yield');
title('Threshold = 0.1');

subplot(2,3,3);
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alpha_yield.t0_2)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alpha_yield.t0_2(1:phase_length);
   alpha_thr(alpha_thr==0) = NaN;
   pl = plot(time_x, alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0 1.1]);
xlabel('Ma');
ylabel('Yield');
title('Threshold = 0.2');

subplot(2,3,4);
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alpha_yield.t0_3)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alpha_yield.t0_3(1:phase_length);
   alpha_thr(alpha_thr==0) = NaN;
   pl = plot(time_x, alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0 1.1]);
xlabel('Ma');
ylabel('Yield');
title('Threshold = 0.3');

subplot(2,3,5);
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alpha_yield.t0_4)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alpha_yield.t0_4(1:phase_length);
   alpha_thr(alpha_thr==0) = NaN;
   pl = plot(time_x, alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0 1.1]);
xlabel('Ma');
ylabel('Yield');
title('Threshold = 0.4');

subplot(2,3,6);
v_labels = cell(length(runs),1);
v_label_items = zeros(length(runs),1);
for rl = 1:length(runs)
   load(runs(rl).name);
   time_x = ((1:1:length(alpha_yield.t0_5)).*alpha_realstep)./1e6; % Ma
   time_x = time_x(1:phase_length);
   alpha_thr = alpha_yield.t0_5(1:phase_length);
   alpha_thr(alpha_thr==0) = NaN;
   pl = plot(time_x, alpha_thr);
   v_label_items(rl) = pl;
   v_labels{rl} = char(dists_order{rl});
   hold on;
end
ylim([0 1.1]);
xlabel('Ma');
ylabel('Yield');
title('Threshold = 0.5');

