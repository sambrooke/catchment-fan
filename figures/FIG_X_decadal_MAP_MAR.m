load('../data/decade_series.mat');

fnames = fieldnames(decade_series_runoff);

fig2 = figure();

X = 10;
Y = 15;
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# fig6ure size on paper (width & height)
ySize = Y - 2*yMargin;     %# fig6ure size on paper (width & height)

set(fig2,'visible','off');
set(fig2, 'PaperSize',[X Y]);
set(fig2, 'PaperPosition',[0 yMargin xSize ySize])
set(fig2, 'PaperUnits','centimeters');

MAP = 0.1;

rainfall_days = [];
average_variance = [];
precip_examples = [];

for j=1:length(fnames)
    dat = decade_series_runoff.(fnames{j});
    rainfall_days = [rainfall_days; dat.events(1,1)];
    average_variance = [average_variance; dat.variance(1,1)];
    precip_examples = [precip_examples; dat.precip_examples(5,:)];
end

sb1 = subplot(2,1,1);
[max_v,max_I] = max(average_variance);
[min_v,min_I] = min(average_variance);
bar(precip_examples(max_I,:));
hold on;
bar(precip_examples(min_I,:));
legend(num2str(round(max_v,5)), num2str(round(min_v,5)));
title(sb1, 'MAP = 0.05 m');
mean_alpha = mean(precip_examples(min_I,:));
plot([0 365], [mean_alpha mean_alpha], 'k-');
xlabel(sb1, 'Day of year');
ylabel(sb1, 'Rainfall (\alpha, m/day)');

sb2 = subplot(2,1,2);
%% Fit: 'untitled fit 1'.
[xData, yData] = prepareCurveData( average_variance, rainfall_days/365 );

% Set up fittype and options.
ft = fittype( 'power1' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [4.72730861059421 -0.556560954488143];

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

h = plot( fitresult, xData, yData);
h(1).MarkerSize = 10;
h(2).Color = 'k';
set(sb2,'xscale','log')
set(sb2,'yscale','log')
xlabel(sb2, 'Variance (\sigma^2)');
ylabel(sb2, 'Chance of rainfall');
text(0.0005, 0.1, [num2str(round(fitresult.a,3)) ...
    'x^{' num2str(round(fitresult.b,3)) '}']);
text(0.0005, 0.07, ['R^2 = ' num2str(gof.rsquare)]);

plot_destination = './pdfs';
run_title = 'MAP_MAR';
print(fig2, [plot_destination filesep run_title '.pdf'],'-dpdf')
