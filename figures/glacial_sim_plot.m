addpath('./');
addpath('./lib');

sims = subdir('../data/glacial_sims/*.mat');


%% DEVILS HOLE
% filename = '../data/devils_hole_o18.csv';
% opts = delimitedTextImportOptions("NumVariables", 2);
% % Specify range and delimiter
% opts.DataLines = [1, Inf];
% opts.Delimiter = ",";
% % Specify column names and types
% opts.VariableNames = ["dh_ka", "dh_o18"];
% opts.VariableTypes = ["double", "double"];
% opts.ExtraColumnsRule = "ignore";
% opts.EmptyLineRule = "read";
% % Import the data
% dh_table = readtable(filename, opts);


%% Plotting functions

% to_plot = {'fixed_mean_variance_0_1', 'fixed_mean_variance_0_2', 'mean_rainfall_no_variance',...
%     'mean_rainfall_with_variance', 'mean_rainfall_with_variance_inverse'};

set(0, 'DefaultFigureVisible', 'off');

for sn = 1:length(sims)
    
    run_file = sims(sn).name;
    load(run_file);
    sims = subdir('../data/glacial_sims/*.mat');

%     vq = interp1(dh_table.dh_ka,dh_table.dh_o18,1:1:150);
%     vq = interp(vq,10)/10;
    X = 15;
    Y = 17;
    xMargin = 0;               %# left/right margins from page borders
    yMargin = 0;               %# bottom/top margins from page borders
    xSize = X - 2*xMargin;     %# fig6ure size on paper (width & height)
    ySize = Y - 2*yMargin;     %# fig6ure size on paper (width & height)

    fig6 = figure('Visible','off');
    set(fig6, 'PaperSize',[X Y]);
    set(fig6, 'PaperPosition',[0 yMargin xSize ySize])
    set(fig6, 'PaperUnits','centimeters');
    
    [ha, pos] = tight_subplot(5, 2, [0.02 0.01], 0.05, [0.1 0.05]);
    
    for nnn = 1:10
        ha(nnn).ActivePositionProperty = 'outerposition';
    end
    
    ha(2).Visible = 'off';
    ha(3).Visible = 'off';
    ha(4).Visible = 'off';
    ha(8).Visible = 'off';
    ha(10).Visible = 'off';
    
    set(ha(6), 'Position', [0.61 0.1 0.25 0.35]);
    set(ha(5), 'Position', [0.1000, 0.33, 0.4200, 0.12]);
    set(ha(1), 'Position', [0.1 0.55 .85 0.41]);
    
    colormap jet;
    % Start at which timestep
    
    t_start_diff = 482;
    total_steps = ss_end+length(alpha);
    
    %plot_t_start = ss_end+1;
    plot_t_start = total_steps-t_start_diff;
    
    ka_slices = [1100, 1200, 1300, 1400]; % For the wheeler and strat plots
    y_plot_start = 1000; % For strat plot
    ka_time_start = 50;  % For time labels
    
    % Original example
%     ha(9).XAxis.MinorTickValues = 0:100:1500;   
%     xticks(0:500:1500);
%     xticklabels((1500:-500:0)/10);   

    sub_plot_x_axis_minor_ticks = 1000:100:1500;
    sub_plot_x_axis_major_ticks = 1000:250:1500;
    sub_plot_x_axis_labels = (500:-250:0)/10;
    
    % Using natural log so we remake grain size with exp()
    gcolour = exp(grainpdf')./max(exp(grainpdf(:)));

    grainpdf_mm = exp(grainpdf');
    max_size = max(max(grainpdf_mm(plot_t_start:end, :)));
    max_size = ceil(max_size);
    max_size = max_size*10;
    max_size = 600;
    extreme_len = ceil(max_size/3);
    extreme_len_1 = floor(extreme_len/3)+1;
    extreme_len_2 = floor(extreme_len/3)+1;
    extreme_len_3 = extreme_len-(extreme_len_1+extreme_len_2)+2;
    
    start_colour = [0.5 0 0];
    intermediate_colour = [0.49 0.18 0.56];
    end_colour = [1 0 1];
    final_colour = [1 0.7 1];
    
    R_1 = linspace(start_colour(1),intermediate_colour(1), extreme_len_1);
    R_1 = R_1(2:end)';
    R_2 = linspace(intermediate_colour(1),end_colour(1), extreme_len_2);
    R_2 = R_2(2:end)';
    R_3 = linspace(end_colour(1),final_colour(1), extreme_len_3)';
    
    G_1 = linspace(start_colour(2),intermediate_colour(2), extreme_len_1);
    G_1 = G_1(2:end)';
    G_2 = linspace(intermediate_colour(2),end_colour(2), extreme_len_2);
    G_2 = G_2(2:end)';
    G_3 = linspace(end_colour(2),final_colour(2), extreme_len_3)';
    
    B_1 = linspace(start_colour(3),intermediate_colour(3), extreme_len_1);
    B_1 = B_1(2:end)';
    B_2 = linspace(intermediate_colour(3),end_colour(3), extreme_len_2);
    B_2 = B_2(2:end)';
    B_3 = linspace(end_colour(3),final_colour(3), extreme_len_3)';
    
    extreme_cmap = [R_1,G_1,B_1;R_2,G_2,B_2;R_3,G_3,B_3];
    cm = jet(max_size-extreme_len);
    cm = [cm;extreme_cmap];
    colormap(cm);

    sg = size(grainpdf_mm);
    % t_end = fs_y;
    % x_end = fs_x;
    t_end = sg(1);
    
    x_end = sg(2);

    % Y step size

    t_step = 50;

    t_start = plot_t_start;

    % X step size
    dx = 50;
    axes(ha(5));

    plot(plot_t_start:1:plot_len,alpha_incident(plot_t_start:end), 'LineWidth', 1);
%     hold on;
%     plot(plot_t_start:1:plot_len,alpha(plot_t_start:end), 'LineWidth', 1);
%     
    ha(5).XAxis.MinorTick = 'on'; % Must turn on minor ticks if they are off
    ha(5).XAxis.MinorTickValues = sub_plot_x_axis_minor_ticks;
    xticks(sub_plot_x_axis_major_ticks);
    
    xticklabels([]);
    
    %xlabel('Time (ka)');
    ylabel('\alpha (m)');
    %legend('Incident Rainfall', 'Effective Rainfall', 'location', 'north',...
    %    'Orientation', 'horizontal');
    ylim([0 0.2]);
    yticks([0 0.1 0.2]);
    xlim([plot_t_start, plot_len]);
    
    hold on;
    yyaxis right
    plot(1:1:plot_len,tbs(ss_end:(ss_end-1+plot_len)), 'm', 'LineWidth', 1.2);
    yticks([0 20 40 60]);
    ylim([0 60]);
    ha(6).XAxis.MinorTick = 'on'; % Must turn on minor ticks if they are off

    grid on;
    axes(ha(7));

    plot(plot_t_start:1:plot_len,alpha_variance(plot_t_start:end), 'LineWidth', 1);
    
    ha(7).XAxis.MinorTick = 'on'; % Must turn on minor ticks if they are off
    ha(7).XAxis.MinorTickValues = sub_plot_x_axis_minor_ticks;
    xticks(sub_plot_x_axis_major_ticks);
    
    xticklabels([]);
    
    %xlabel('Time (ka)');
%     ylabel('\sigma^2');
%     ylim([0 4]);
%     xlim([plot_t_start, plot_len]);
%     yticks([0 1 2 3 4]);
%     yticklabels([0 1 2 3 4]);
%     yyaxis right;
%     vq_flip = fliplr(vq);
%     plot(plot_t_start:1:length(vq),vq_flip(plot_t_start:end), '-.', 'Color', ...
%         [0.8500    0.3300    0.1000], 'LineWidth', 1);
%     grid on;
    axes(ha(9));
    
    plot(plot_t_start:1:plot_len,erodq(plot_t_start+ss_end:end), 'LineWidth', 1);

    ha(9).XAxis.MinorTick = 'on'; % Must turn on minor ticks if they are off
    ha(9).XAxis.MinorTickValues = sub_plot_x_axis_minor_ticks; 
    xticks(sub_plot_x_axis_major_ticks);
    xticklabels(sub_plot_x_axis_labels);
    
    xlabel('Time (ka)');
    ylabel('qs m^2ka^{-1}');
    yticks([0:0.5:3.5]);
    yticks([0:1:3]);
    ylim([0 3.5]);
    xlim([plot_t_start, plot_len]);
    %yticks([500 1000 1500 2000 2500 3000 3500 4000]);
    grid on;
    axes(ha(1));
    fan_heights = fan_elevation;
    fan_x = coord_basin*lx;
    fan_dx = fan_x(x_end)/max(x_end);
    ss_aquisition = plot_t_start;

    alpha_x = 1:1:length(alpha);
    alpha_x = alpha_x.*alpha_realstep./1e3;
    alpha_x = alpha_x((plot_t_start):end);

    maxPrecip = max(alpha);
    maxErosion = max(erodq(t_start:end));

    title('Fan architecture');
    set(ha(7), 'Position', [0.1000 0.215 0.4200 0.1]);
    set(ha(9), 'Position', [0.1000 0.11 0.4200 0.09]);
%     a= annotation('textbox', [0 0.89 1 0.1], ...
%         'String', run_title, ...
%         'EdgeColor', 'none', ...
%         'HorizontalAlignment', 'center');
%     set(a,'interpreter','none')

    for y=t_start:t_step:t_end

        y_b = y;        y_t = y+t_step;
        if y_t > t_end
            y_t = t_end;
        end

        for x=1:dx:x_end
            x_l = x;
            x_r = x+dx;

            if x_r > x_end
                x_r = x_end;
            end

            % SWAPPED fan_elevation(t,:) to fan_elevation(:,t)
            xco = [fan_x(x_l) fan_x(x_r) fan_x(x_r) fan_x(x_l)];
            zco = [fan_heights(x, y_t), fan_heights(x_r,y_t), fan_heights(x_r,y_b), fan_heights(x,y_b)];

            colourID = mean(mean(grainpdf_mm(y_b:y_t,x_l:x_r),'omitnan'),'omitnan');
            %colourID = max(max(grainpdf_mm(y_b:y_t,x_l:x_r)));

            if isnan(colourID)
                colour = [1 1 1];
            else
                color_ceil = ceil(colourID*10);
                if color_ceil > max_size
                    colour = cm(end,:);
                else
                    colour = cm(color_ceil,:);
                end
            end

            if sum(colour) == 0
               disp('No colour!');
            end
            h = patch(xco, zco, colour, 'EdgeColor', 'none');
            drawnow
        end

    end

%     marker_gs = [100, 50, 30];
% 
%     for mgs = 1:length(marker_gs)
%         x_coords = [];
%         y_coords = [];
%         for y=ss_end+1:1:t_end
%             [cvv index] = min(abs(grainpdf_mm(y,:)-marker_gs(mgs)))
% 
%             x_coords = [x_coords; fan_x(index)];
%             y_coords = [y_coords; fan_heights(index, y)];
%         end
% 
%         line(x_coords, y_coords,'LineWidth',0.5, 'Color', [0 0 0]);
%     end

    %Time Slices (thousands of years)

    for kslice = 1:length(ka_slices)
        ka_line = fan_heights(:, ka_slices(kslice));
        line(1:fan_dx:fan_x(x_end), ka_line,'LineWidth',0.5, 'Color', [0 0 0]);    
    end

    ss_realstep = lx^2/kappa*ss_dt;
    alpha_realstep = lx^2/kappa*alpha_dt;
    real_alpha_max = alpha_realstep * full_duration;
    real_ss_max = ss_realstep*plot_t_start;

    xlabel('Metres');
    ylabel('Metres');
    ylim([-1400,400]);
    yticks(-1400:200:400);
    yticklabels(-1400:200:400);
    xticks(0:500:3000);
    xticklabels(0:500:3000);
    
    custom_lines = [t_start, t_end];

    for ts = 1:length(custom_lines)
        cs_line = fan_heights(:, custom_lines(ts));
        line(1:fan_dx:fan_x(x_end), cs_line,'LineWidth',1.3, 'Color', [0 0 0]);
    end

    run_params = {['\bf kappa', '\rm ',  num2str(kappa), '\bf   ss time  ', '\rm ',  [num2str(real_ss_max/1e6) 'Ma']], ...
        ['\bf n ', '\rm ',  num2str(nexp), '\bf   run time  ', '\rm ',  [num2str(real_alpha_max/1e3) 'kyr']], ...
        ['\bf c  ', '\rm ',  num2str(c)], ...
        ['\bf catchment uplift  ', '\rm ',  num2str(u0), 'my^{-1}'], ...
        ['\bf catchment length  ', '\rm ',  num2str(lx) 'm']
    };

    grid on;
    
    axes(ha(6));
    
    imagesc(grainpdf_mm(plot_t_start:end,:));
    caxis([0 max_size/10]);
    title('Grain Size Fronts');
    ylabel('Time (ka)');
    yticks_wheeler = 0:100:t_start_diff;
    yticks(yticks_wheeler);
    yticklabels(ka_time_start:-10:0);
    xticks(1:500:1000);
    xticklabels(0:1500:3000);
    xlim([0, 1001])
    ylim([0, t_start_diff+1])
    grid on;
    hold on;
    
    for ks= 1:length(ka_slices)
        plot([0 1000], [ka_slices(ks)-plot_t_start ka_slices(ks)-plot_t_start], 'k-', 'LineWidth', 1.2);
        hold on;
    end
    
    xlabel('Distance (m)');
    hcb=colorbar;
    set(hcb,'YTick', 0:floor(((max_size/10)/6)):max_size);
    set(hcb,'YTickLabel',linspace(0,ceil(max_size/10),7));
    ylabel(hcb,'D_{50} Grain Size (mm)');

    set(ha(6), 'Position', [0.61 0.1 0.25 0.35]);
    fig6.Renderer = 'Painters';
    plot_destination = './pdfs';

      
    for j = 1:length(ha)
        set(ha(j), 'FontSize', 10);
    end
    plot_destination = './'
    print(fig6, [plot_destination filesep run_title '.pdf'],'-dpdf')

end
plot_t_start